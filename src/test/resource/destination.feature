@myDestinations 
Feature: Verify My Destinations tab links for teacher
	Description: This feature will test all the links present under My Destinations tab for teacher.

Background: 
	Given user logIn with valid credentials 
		| username                 | password    |
		| email.teacher_true@rl.com | passed1 |
		
		
@VerifyDestinationLink 
Scenario: Verify Links present under destination tab for teacher
	When verify the destination link 
		|https://clubs2.scholastic.com/webapp/wcs/stores/servlet/LogonForm                  |
		|https://www.scholastic.com/bookfairs/                                              |
		|http://shop.scholastic.com/webapp/wcs/stores/servlet/StoreView?storeId=10751       |
		|http://www.scholastic.com/bookwizard/                                              |
		|http://www.scholastic.com/storia-school                                            |
		|http://www.scholastic.com/teachers/lesson-plans/free-lesson-plans                  |
		|http://printables.scholastic.com/printables/home                                   |
		|http://www.scholastic.com/teachers/teaching-ideas                                  |
