@forgotPwd
Feature: Forgot Password 
	Description:This feature will test the forgot password functionality
Background: 
	Given user is on My Account2 home page 

	
@retrievePwd
Scenario: User should be able create new password using valid email 
	When User enters valid email "sarvjeet.singh@relevancelab.com" 
	Then confirmation message "Account information sent" displays 
	
	
@retrieveEmail
Scenario: User should be able to retrieve email using name and school details 
	When User provides valid name and school details 
		|fName|lName|state|city|school|
		|test1|singh|NY|New York|23006422|
	Then user gets their email and choose to create new password
	And confirmation message "Account information sent" displays 
	
	
@retrieveEmailUsingZipCode 
Scenario: User should be able to retrieve email using name and zip code 
	When User provides valid name and zip code 
		|fName|lName|zipCode|school|
		|test1|singh|10001|23006422|
	Then user gets their email and choose to login
	And User gets the login page