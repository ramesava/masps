@address 
Feature: Address
	Description: This feature will test the add/edit/delete address functionality. Maximum five address can be added.
	After adding five address you will not get option to add another address.

Background: 	
	Given user login with valid credentials 
		| username                 | password    |
		| email.teacher_true@rl.com | passed1 |
	
		
		
@editSchoolAddress 
Scenario: Edit school address 
	
	When user edits the school address 
		| SchoolName                 | addressLine1 | addressLine2 | zipCode |
		| SSB International School   | Indranagar   | bangalore    | 10001   |
	Then user should comes back to address page
		
		
@addAddress 
Scenario: Add new address 
	
	When user adds the new address 
		| Description | Fname    | Lname | addressLine1 | addressLine2 | zipCode |
		| Home        | Sarvjeet | Singh | Indranagar   | bangalore    | 10001   |
	Then user should comes back to address page
		
@editAddress 
Scenario: Edit address 
	
	When user edits the address 
		| Description | Fname    | Lname | addressLine1         | addressLine2       | zipCode |
		| Home        | Sarvjeet | Singh | 748 N Mathilda Ave   | Room Number 254    | 94085   |
	Then user should comes back to address page		
			
@deleteAddress 
Scenario: delete address 
	
	When user deletes the first address 
	Then user should comes back to address page