@login
Feature: Login
Description: This feature will test the login functionality for both valid and invalid users. 

Background: 
	Given user is on My Account2 home page 
	
@valid
Scenario: Verify the login functioanlity for valid email and password
	When user logs in with their valid email and password 
		| username                 | password    |
		| email.teacher_true@rl.com | passed1 |
	Then my profile page appears 
	And user verifyies their email "email.teacher_true@rl.com"
	
	

@invalid
Scenario: Verify the login functioanlity for valid email and invalid password
	When user enters valid email and invalid password
		| email                    | pwd    |
		|email.teacher_test90@rl.com|pass |
	Then page displayes error message

	
	
	
	