@myProfile 
Feature: My Profile 
	Description: This feature will test edit name, edit password, edit email, edit role, edit profile question, add child
verify all the tabs under My Account.

Background: 
	Given user log in with valid credential 
		| username                 | password    |
		| email.teacher_true@rl.com | passed1 |
		
		
@pwdEdit 
Scenario: Login to application and change password 

	When user changes his password 
		| cpwd        | npwd        |
		| passed1 | passed1 |
	Then user should comes back to my profile page
		
		
@nameEdit 
Scenario: Login to application and change Name 
	When user changes his Name 
		| Title | Fname | LName |
		| MR    | Jeet  | Kumar |
	Then user should comes back to my profile page	
	
		
@emailEdit 
Scenario: Login to application and edit email 
	When user edits his email 
		| nEmail 					| cEmail 				   |
		| email.teacher_true@rl.com  | email.teacher_true@rl.com |
	Then user should comes back to my profile page	
		
		
@roleEdit 
Scenario: Login to application and edit role 
	When user edits role and class size as "20" 
	Then user should comes back to my profile page
	
	
@profileQEdit 
Scenario: Login to application and edit profile questions 
	When user edits the profile questions 
	Then user should comes back to my profile page
	
	
@addChild 
Scenario: Add child details 
	When user enters the child details 
		| Fname    | Lname   | Grade | Month | Year | CAC   |
		| Abhijeet | Chauhan | 98    | 00    | 2012 | TV94C |
	Then user should comes back to my profile page	

		
@editChild 
Scenario: Edit child details 
	When user edits the child details 
		| Fname    | Lname   | Grade | Month | Year | CAC   |
		| Abhijeet1 | Chauhan1 | 01    | 01    | 2011 | TV94C |		
	Then user should comes back to my profile page	
	
		
@deleteChild 
Scenario: Delete child details 
	When user delete the child details 
	Then user should comes back to my profile page
	
	
@verifyLink 
Scenario: Verify links under MyAccount 
	When user verifies myAccount links 
		| My Profile         |
		| Addresses          |
		| Order History      |
		| Billing & Payments |
		| Subscriptions      |
		| My Destinations    |
		| My Downloads       |
		
		
