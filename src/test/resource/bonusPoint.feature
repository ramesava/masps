@bonusPoint
Feature: Verify Bonus Point functionality 
	Description: This feature will test Bonus Point functionality.
	
@BP
Scenario: User will check their bonus point balance
	
	Given user is loged in with valid email "27selfbtsuser111@scholastic.com" and password "passed1" 
	When user goes to bonus point page
	Then user checks their bonus point balance
	