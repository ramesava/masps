@orderHistory 
Feature: Order History 
	Description: This feature will test view/track order details functionality.
	

Background: 	
	Given user login with valid email "testcoolt3@mail.com" and password "passed1" 
	And user is on order history page

			
@viewOrderDetail 
Scenario: User will check their order through view Order Details link
	
	When user go to view order details page 
	Then user verifies their order
	And returns to order history page

			
@trackOrder 
Scenario: User will track their order through order number link
	
	When user go to order number page
	Then user verifies their order
	And returns to order history page