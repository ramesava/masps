@creditCard 
Feature: Billing and Payments

Description:

This feature will test the billing and payments functionality. User should be able to perform 
following actions: add/edit/set as default/delete credit card and makePayment from billing and payments section.


Background: 
	Given user logs in with valid credentials 
		| username                 | password     |
		| 27selfbtsuser111@scholastic.com | passed1 |
	And user goes to Billing and Payments page 
	
	
@addCard 
Scenario: Add credit card 
	When user fills valid data in add new credit card form 
		| cardType | cardNo           | expMonth | expYear | securityCode | Fname | Lname    | addressLine1 | addressLine2 | zipCode |
		| V        | 4111111111111111 | 01       | 2019    | 123         | Singh | sarvjeet | indranagar   | bangalore    | 10012   |
	Then user verifies the card status 
	
	
@editCard 
Scenario: Edit credit card 
	When user edits credit card details with valid data 
		| expMonth | expYear | Fname | Lname    | addressLine1 | addressLine2 | nEmail                        |
		| 01       | 2019    | Singh | sarvjeet | indranagar   | bangalore    | sarvjeetkumar_singh@yahoo.com |
	Then user verifies the card status 
	
	
@setDefaultCard 
Scenario: set card as default 
	When user sets their credit card as default 
	Then user verifies the card status 
	
	
@deleteCard
Scenario: delete credit card 
	When user delete their credit card 
	Then user verifies the card status 
	
@makePayment @ignore
Scenario: Make Payment 
	When user goe to make payment page 
	And user makes the payment "0.5" through their credit card 
	Then user fills valid data in add temp credit card form 
		| cardType | cardNo           | expMonth | expYear | securityCode | Fname | Lname    | addressLine1 | addressLine2 | zipCode |nEmail                        |
		| V        | 4111111111111111 | 01       | 2019    | 123         | Singh | sarvjeet | indranagar   | bangalore    | 10012   |sarvjeetkumar_singh@yahoo.com |
	And user confirms their payment "Confirm Your Payment" 
	And user gets payment confirmation message "Payment Confirmation" 
	
	
