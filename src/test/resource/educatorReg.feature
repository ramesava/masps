@EducatorReg 
Feature: Educator Registration 
	Description:This feature will test the educator registration functionality.

Background: 
	Given user is on educator registration page 
	
	
@EduRegUSSchool 
Scenario: 
	Create an account for educator with school location United States School and select a school from the list 
	
	When user fills valid data in educator personal details page 
		| Title | FName    | Lname | Email                      | Cemail                     | pwd     | cpwd    | 
		| MR    | Teacher1 | test1 | A1email.teacher_test10@rl.com | A1email.teacher_test10@rl.com | passed1 | passed1 | 
	And provides school details with his roles and class size 
		|state | city   | size |
		| AK    | Akiak | 12   |
	Then Verify the teacher Registration confirmation page 
		| message                   |
		| Thank you for registering. |
		
		
@EduRegManualSchoolEntry 
Scenario: 
	Create an account for educator with school location United States school and fill manual school entry form to select a school
	
	When user fills valid data in educator personal details page 
		| Title | FName    | Lname | Email                      | Cemail                     | pwd     | cpwd    | 
		| MR    | Test | test | A2email.teacher_test10@rl.com | A2email.teacher_test10@rl.com | passed1 | passed1 | 
	And fills valid data in manual school entry form with roles and class size 
		|state | city   |SchoolName  | SchoolType| SchoolAddr1|city |ZipCode|Phone        |size |
		| AK    | Akiak | PlaySchool | EC        |  NYC       |Akiak|10012 | 817 569 8900 | 12  |
		
	Then Verify the teacher Registration confirmation page 
		| message                   |
		| Thank you for registering. |	
		
		
@EduRegMilitaryBase 
Scenario: 
	Create an account for educator with school location Military Base and select school from the list based on zip code provided
	
	When user fills valid data in educator personal details page 
		| Title | FName    | Lname | Email                      | Cemail                     | pwd     | cpwd    | 
		| MR    | Teacher1 | test1 | A3email.teacher_test10@rl.com | A3email.teacher_test10@rl.com | passed1 | passed1 | 
	And select school based on zip code from the list and provide details about roles and class size 
		|zipCode | size |
		| 09169  | 12   |
	Then Verify the teacher Registration confirmation page 
		| message                   |
		| Thank you for registering. |		
		

@EduRegHomeSchool 
Scenario: 
	Create an account for educator with school location Home School
	
	When user fills valid data in educator personal details page 
		| Title | FName    | Lname | Email                      | Cemail                     | pwd     | cpwd    | 
		| MR    | Teacher1 | test1 | A4email.teacher_test10@rl.com | A4email.teacher_test10@rl.com | passed1 | passed1 | 
	And Select home school option and fills manual school entry form with roles and class size 
		|SchoolName  | SchoolAddr1|city |State|ZipCode|Phone        |
		| PlaySchool |  NYC       |Akiak| AK  | 10012 | 817 569 8900 | 
	Then Verify the teacher Registration confirmation page 
		| message                   |
		| Thank you for registering. |			
		
				
@EduRegNonUSSchool 
Scenario: 
	Create an account for educator with school location Non US School
	
	When user fills valid data in educator personal details page 
		| Title | FName    | Lname | Email                      | Cemail                     | pwd     | cpwd    | 
		| MR    | Teacher4 | test4 | A6email.teacher_test10@rl.com | A6email.teacher_test10@rl.com | passed1 | passed1 | 
	And Select non US school option and fills manual school entry form with roles and class size 
		|Country|SchoolName  |SchoolType| SchoolAddr1|city    |State       |ZipCode |Phone       |size |Country1|
		| AL    | PlaySchool |ISI       |  Indranagar|BNGALORE| KARNATAKA  | 560038 |9845012351   | 12  |AF|
	Then Verify the teacher Registration confirmation page 
		| message                   |
		| Thank you for registering. |		