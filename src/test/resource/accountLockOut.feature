@AcctLockout 
Feature: Account Lock out test 


Scenario Outline: Account lockout functionality 
	Given user is on MA2 home page 
	When User enters "<username>" and "<password>" 
	Then error message displayes 
	
	
	Examples: 
		| username                  | password |
		| email.teacher_true@rl.com | 123      | 
		| email.teacher_true@rl.com | 123      | 
		| email.teacher_true@rl.com | 123      |
		| email.teacher_true@rl.com | 123      |
		| email.teacher_true@rl.com | 123      |
		| email.teacher_true@rl.com | 123      |
		| email.teacher_true@rl.com | 123      |
		| email.teacher_true@rl.com | 123      |
		| email.teacher_true@rl.com | 123      |
		| email.teacher_true@rl.com | 123      |
		
		
