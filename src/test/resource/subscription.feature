@subscription
Feature: Verify subscription functionality 
	Description: This feature will test subscription details and edit billing info functionality.
	

Background: 	
	Given user log-in with valid email "testcoolt3@mail.com" and password "passed1" 
	And user is on subscription page

			
@editBilingInfo
Scenario: User will edit the billing info 
	
	When user edits billing info with email "testcoolt3@mail.com"
	Then user returns to subscription home page
	

			
@viewSubscription
Scenario: User will verify their subscription detail
	
	When user go to their subscription page	
    And user edits billing info with email "testcoolt3@mail.com"
	Then user verifies subscription home page
