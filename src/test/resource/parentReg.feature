@parentReg
Feature: Parent Registration 
Description:This feature will test the parent registration functionality.

Background: 
	Given user is on parent registration page
	
@ParentReg 
Scenario: Create an account for parent and verify the registration confirmation page
	
	When user fills valid data in the parent registration form 
		| Title | FName   | Lname | Email                     | Cemail                    | pwd         | cpwd        | cFname   | cLName |
		| MR    | Parent | Test1  | 25email.parent_test20@rl.com | 25email.parent_test20@rl.com | passed1     | passed1     | child1   | test1  |
	Then Verify the parent Registration confirmation page
		| message                    |
		| Thank you for registering. |