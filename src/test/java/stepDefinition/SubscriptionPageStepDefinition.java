package stepDefinition;

import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import Pages.Subscription;
import Pages.USSchoolList;
import Pages.pwdChange;
import Util.WebDriverCommonLib;
import Util.log4jLib;
import Util.webConnect;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * @author Sarvjeet
 */
public class SubscriptionPageStepDefinition extends webConnect {

	WebDriver driver = getDriver();
	Logger log = log4jLib.getLog(SubscriptionPageStepDefinition.class);

	@Given("^user log-in with valid email \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void user_log_in_with_valid_email_and_password(String email, String pwd) throws Throwable {

		pwdChange pwdCnge = PageFactory.initElements(driver, pwdChange.class);
		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		USSchoolList edReg = PageFactory.initElements(driver, USSchoolList.class);

		pwdCnge.loginId(email);
		pwdCnge.LoginPwd(pwd);
		pwdCnge.nxtBtn();
		cLib.assertDisplayed(edReg.nameWel());

	}

	@Given("^user is on subscription page$")
	public void user_is_on_subscription_page() throws Throwable {

		Subscription sub = PageFactory.initElements(driver, Subscription.class);
		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);

		sub.subTab();
		cLib.waitForPageToLoad();

	}

	@When("^user edits billing info with email \"([^\"]*)\"$")
	public void user_edits_billing_info_with(String email) throws Throwable {

		Subscription sub = PageFactory.initElements(driver, Subscription.class);

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);

		sub.bilingInfoEdt();
		cLib.waitForPageToLoad();
		sub.creditCrdRadio();
		sub.email(email);
		sub.saveBtn();
		cLib.waitForPageToLoad();

	}

	@Then("^user returns to subscription home page$")
	public void user_returns_to_subscription_home_page() throws Throwable {

		Subscription sub = PageFactory.initElements(driver, Subscription.class);
		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		
		cLib.waitForDisplayed(sub.SubText());
		log.info("User is able to edit the subscription");
		logout();

	}

	@When("^user go to their subscription page$")
	public void user_go_to_their_subscription_page() throws Throwable {

		Subscription sub = PageFactory.initElements(driver, Subscription.class);

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);

		sub.subPage();
		cLib.waitForDisplayed(sub.subStatus());
		log.info("User is subscribed to: " + sub.subStatusTxt());
	}
	

	@Then("^user verifies subscription home page$")
	public void user_verifies_subscription_home_page() throws Throwable {
		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		Subscription sub = PageFactory.initElements(driver, Subscription.class);
		
		cLib.waitForDisplayed(sub.SubText());
		log.info("User is able to verify and edit the subscription");
		logout();

	}
	
	
	@After("@editBilingInfo,@viewSubscription")
	public void embedScreenshot(Scenario scenario) {

		if (scenario.isFailed()) {
			try {
				scenario.write("Current Page URL is " + driver.getCurrentUrl());
				byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
				scenario.embed(screenshot, "image/png");
			} catch (org.openqa.selenium.WebDriverException wde) {
				System.err.println(wde.getMessage());
			} catch (ClassCastException cce) {
				cce.printStackTrace();
			}

		}
	}
	
	

}
