package stepDefinition;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import Pages.AccountLock;
import Pages.pwdChange;
import Util.ScreenShot;
import Util.WebDriverCommonLib;
import Util.log4jLib;
import Util.webConnect;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * @author Sarvjeet
 */
public class AcctLockStepDefinition extends webConnect {

	WebDriver driver = getDriver();
	Logger log = log4jLib.getLog(AcctLockStepDefinition.class);

	@Given("^user is on MA2 home page$")
	public void user_is_on_MA2_home_page() throws Throwable {
		log.info("User is on My Account2 home page");

	}

	@When("^User enters \"(.*)\" and \"(.*)\"$")
	public void user_enters_UserName_and_Password(String username, String password) throws Throwable {

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);

		pwdChange pwdCnge = PageFactory.initElements(driver, pwdChange.class);

		pwdCnge.loginId(username);
		pwdCnge.LoginPwd(password);
		pwdCnge.nxtBtn();
		cLib.pause(3000);

	}

	@Then("^error message displayes$")
	public void message_displayed() throws Throwable {

		AccountLock act = PageFactory.initElements(driver, AccountLock.class);

		try {
			if (act.invalidWeb().isDisplayed()) {
				log.info(act.invalid());
			}

			if (act.actLkdL().size() != 0 && act.actLkdWeb().isDisplayed()) {
				log.info(act.actLkd());
			}
		} catch (Exception e) {

			ScreenShot ss = new ScreenShot(driver);
			ss.TestScreenShots("AccountLckError");
			log.error("Error in account lockout functionality");
			e.printStackTrace();
		}

	}

}
