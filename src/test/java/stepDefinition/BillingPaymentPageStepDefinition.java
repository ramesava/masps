package stepDefinition;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import Pages.MakePayment;
import Pages.USSchoolList;
import Pages.addCard;
import Pages.editCard;
import Pages.pwdChange;
import Util.ScreenShot;
import Util.WebDriverCommonLib;
import Util.log4jLib;
import Util.webConnect;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * @author Sarvjeet
 */
public class BillingPaymentPageStepDefinition extends webConnect {

	WebDriver driver = getDriver();
	Logger log = log4jLib.getLog(BillingPaymentPageStepDefinition.class);

	@Given("^user logs in with valid credentials$")
	public void user_logs_in_with_valid_credentials(DataTable usercredentials) throws Throwable {

		List<List<String>> data = usercredentials.raw();
		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		pwdChange pwdCnge = PageFactory.initElements(driver, pwdChange.class);
		USSchoolList edReg = PageFactory.initElements(driver, USSchoolList.class);

		pwdCnge.loginId(data.get(1).get(0));
		pwdCnge.LoginPwd(data.get(1).get(1));
		pwdCnge.nxtBtn();
		cLib.assertDisplayed(edReg.nameWel());

	}

	@Given("^user goes to Billing and Payments page$")
	public void user_goes_to_Billing_and_Payments_page() throws Throwable {

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		editCard edtCrd = PageFactory.initElements(driver, editCard.class);

		edtCrd.billTab();
		cLib.waitForPageToLoad();

	}

	@When("^user fills valid data in add new credit card form$")
	public void user_fills_valid_data_in_add_credit_card_form(DataTable addCard) throws Throwable {

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		List<List<String>> data = addCard.raw();
		addCard addCrd = PageFactory.initElements(driver, addCard.class);

		cLib.waitForPageToLoad();
		addCrd.addCrd();
		cLib.waitForPageToLoad();
		addCrd.cardType(data.get(1).get(0));
		addCrd.cardNumber(data.get(1).get(1));
		addCrd.eMonth(data.get(1).get(2));
		addCrd.eYear(data.get(1).get(3));
		addCrd.cCVN(data.get(1).get(4));
		addCrd.fName(data.get(1).get(5));
		addCrd.lName(data.get(1).get(6));
		addCrd.adrLine1(data.get(1).get(7));
		addCrd.adrLine2(data.get(1).get(8));
		addCrd.postalCode(data.get(1).get(9));
		addCrd.ctyState();
		cLib.assertDisplayed(addCrd.stateDsp());
		addCrd.svBtn();
		cLib.waitForPageToLoad();
		cLib.assertDisplayed(addCrd.addCrdLnk());
		log.info("User is able to add credit card");

	}

	@When("^user edits credit card details with valid data$")
	public void user_edits_credit_card_details_with_valid_data(DataTable editCard) throws Throwable {

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		List<List<String>> data = editCard.raw();
		addCard addCrd = PageFactory.initElements(driver, addCard.class);
		editCard edtCrd = PageFactory.initElements(driver, editCard.class);

		edtCrd.edtcard();
		cLib.waitForPageToLoad();
		edtCrd.eMonth(data.get(1).get(0));
		edtCrd.eYear(data.get(1).get(1));
		edtCrd.fName(data.get(1).get(2));
		edtCrd.lName(data.get(1).get(3));
		edtCrd.adrLine1(data.get(1).get(4));
		edtCrd.adrLine2(data.get(1).get(5));
		edtCrd.nEmail(data.get(1).get(6));
		cLib.waitForPageToLoad();
		edtCrd.svBtn();
		cLib.waitForPageToLoad();
		cLib.assertDisplayed(addCrd.addCrdLnk());
		log.info("User is able to edit credit card");

	}

	@Then("^user verifies the card status$")
	public void user_verifies_the_card_status() throws Throwable {

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		addCard addCrd = PageFactory.initElements(driver, addCard.class);

		if (addCrd.addCrdLnk1().size() != 0) {

			cLib.assertDisplayed(addCrd.addCrdLnk());
			logout();
		}

		else {
			ScreenShot ss = new ScreenShot(driver);
			ss.TestScreenShots("CreditCardError");
			log.error(addCrd.error());
		}

	}

	@When("^user delete their credit card$")
	public void user_delete_their_credit_card() throws Throwable {

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		addCard addCrd = PageFactory.initElements(driver, addCard.class);

		addCrd.delete();
		cLib.pause(3000);
		cLib.alertAccept();
		cLib.assertDisplayed(addCrd.addCrdLnk());
		log.info("User is able to delete credit card");

	}

	@When("^user sets their credit card as default$")
	public void user_sets_their_credit_card_as_default() throws Throwable {

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		addCard addCrd = PageFactory.initElements(driver, addCard.class);

		addCrd.setDefalut();
		cLib.waitForPageToLoad();
		cLib.assertDisplayed(addCrd.deafault());
		log.info("User is able to set card as a default");
	}

	@When("^user goe to make payment page$")
	public void user_goe_to_make_payment_page() throws Throwable {

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		MakePayment mkPmt = PageFactory.initElements(driver, MakePayment.class);

		float i = Float.parseFloat(mkPmt.balance().replaceAll("[$]", ""));

		try {
			if (i > 0) {
				log.info("Total Balance in this Account is: " + i);
				mkPmt.mPaymentBtn();
				cLib.assertDisplayed(mkPmt.bilingWeb());
			}

			else {
				log.info("Make Payment option is not enabled as the Balance is: " + i);
			}

		} catch (Exception e) {
			log.error(e.getMessage());

		}

	}

	@And("^user makes the payment \"([^\"]*)\" through their credit card$")
	public void user_makes_the_payment_through_their_credit_card(String amt) throws Throwable {

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		MakePayment mkPmt = PageFactory.initElements(driver, MakePayment.class);

		mkPmt.partialBtn();
		mkPmt.partilal(amt);
		mkPmt.addCrdLnk();
		cLib.waitForPageToLoad();

	}

	@And("^user confirms their payment \"([^\"]*)\"$")
	public void user_confirms_their_payment(String msg) throws Throwable {

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		MakePayment mkPmt = PageFactory.initElements(driver, MakePayment.class);

		cLib.assertTextMatches(mkPmt.bilingCnfWeb(), msg);
		mkPmt.bilingCnfCmpBtn();
		cLib.waitForPageToLoad();

	}

	@And("^user gets payment confirmation message \"([^\"]*)\"$")
	public void user_gets_payment_confirmation_message(String msg) throws Throwable {

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		MakePayment mkPmt = PageFactory.initElements(driver, MakePayment.class);

		cLib.assertTextMatches(mkPmt.paymentCnfWeb(), msg);
		mkPmt.bilingBkBtn();
		cLib.assertDisplayed(mkPmt.addCrdLnk1());
		log.info("Make Payment completed");

	}

	@When("^user fills valid data in add temp credit card form$")
	public void user_fills_valid_data_in_add_temp_credit_card_form(DataTable addCard) throws Throwable {

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		List<List<String>> data = addCard.raw();
		addCard addCrd = PageFactory.initElements(driver, addCard.class);

		cLib.waitForPageToLoad();
		addCrd.cardType(data.get(1).get(0));
		addCrd.cardNumber(data.get(1).get(1));
		addCrd.eMonth(data.get(1).get(2));
		addCrd.eYear(data.get(1).get(3));
		addCrd.cCVN(data.get(1).get(4));
		addCrd.fName(data.get(1).get(5));
		addCrd.lName(data.get(1).get(6));
		addCrd.adrLine1(data.get(1).get(7));
		addCrd.adrLine2(data.get(1).get(8));
		addCrd.postalCode(data.get(1).get(9));
		addCrd.ctyState();
		cLib.assertDisplayed(addCrd.stateDsp());
		addCrd.nEmail(data.get(1).get(10));
		addCrd.svBtn();
		cLib.waitForPageToLoad();

	}

	@After("@addCard,@editCard,@setDefaultCard,@deleteCard,@makePayment")
	public void embedScreenshot(Scenario scenario) {

		if (scenario.isFailed()) {
			try {
				scenario.write("Current Page URL is " + driver.getCurrentUrl());
				byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
				scenario.embed(screenshot, "image/png");
			} catch (org.openqa.selenium.WebDriverException wde) {
				System.err.println(wde.getMessage());
			} catch (ClassCastException cce) {
				cce.printStackTrace();
			}

		}
	}
}
