package stepDefinition;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import Pages.USSchoolList;
import Pages.pwdChange;
import Util.ScreenShot;
import Util.WebDriverCommonLib;
import Util.log4jLib;
import Util.webConnect;

import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * @author Sarvjeet
 */
public class LoginPageStepDefinition extends webConnect {

	WebDriver driver = getDriver();
	Logger log = log4jLib.getLog(LoginPageStepDefinition.class);

	@Given("^user is on My Account2 home page$")
	public void user_is_on_My_Account2_home_page() throws Throwable {
		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		cLib.pause(3000);
		log.info("User is on My Account2 home page");

	}

	@When("^user logs in with their valid email and password$")
	public void user_logs_in_with_their_valid_email_and_password(DataTable usercredentials) throws Throwable {

		List<List<String>> data = usercredentials.raw();
		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		pwdChange pwdCnge = PageFactory.initElements(driver, pwdChange.class);
		USSchoolList edReg = PageFactory.initElements(driver, USSchoolList.class);

		pwdCnge.loginId(data.get(1).get(0));
		pwdCnge.LoginPwd(data.get(1).get(1));
		pwdCnge.nxtBtn();
		cLib.assertDisplayed(edReg.nameWel());
		cLib.pause(2000);
	}

	@Then("^my profile page appears$")
	public void my_profile_page_appears() throws Throwable {

		USSchoolList edReg = PageFactory.initElements(driver, USSchoolList.class);
		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);

		try {
			// AssertUtils.assertDisplayed(edReg.nameWel());
			cLib.assertDisplayed(edReg.nameWel());
			log.info("User logged in to MA2");
		}

		catch (Exception e) {
			log.error("User is failed to login with valid email and password");
			ScreenShot ss = new ScreenShot(driver);
			ss.TestScreenShots("LoginFailed");
			log.error(e.getMessage());
		}
	}

	@And("^user verifyies their email \"([^\"]*)\"$")
	public void user_verifyies_their_email_something(String email) throws Throwable {

		USSchoolList edReg = PageFactory.initElements(driver, USSchoolList.class);
		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);

		// AssertUtils.assertTextMatches(edReg.emailWeb(),
		// Matchers.equalToIgnoringCase(email));
		cLib.assertTextMatches(edReg.emailWeb(), email);
		log.info("User verified their email");
		logout();
	}

	@When("^user enters valid email and invalid password$")
	public void user_enters_valid_email_and_invalid_password(DataTable formData) throws Throwable {

		List<List<String>> data = formData.raw();
		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		USSchoolList edReg = PageFactory.initElements(driver, USSchoolList.class);

		edReg.nUsrEmail(data.get(1).get(0));
		edReg.nUsrPwd(data.get(1).get(1));
		edReg.nxtBtn();
		cLib.pause(2000);
	}

	@Then("^page displayes error message$")
	public void page_displayes_error_message() throws Throwable {

		USSchoolList edReg = PageFactory.initElements(driver, USSchoolList.class);
		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);

			log.info("User enters wrong credentials");
			cLib.assertDisplayed(edReg.signInWeb());
			log.error("Error message: " + edReg.signInError());
			ScreenShot ss = new ScreenShot(driver);
			ss.TestScreenShots("InvalidCredentials");
	}
	
	@After("@valid,@invalid")
	public void embedScreenshot(Scenario scenario) {

		if (scenario.isFailed()) {
			try {
				scenario.write("Current Page URL is " + driver.getCurrentUrl());
				byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
				scenario.embed(screenshot, "image/png");
			} catch (org.openqa.selenium.WebDriverException wde) {
				System.err.println(wde.getMessage());
			} catch (ClassCastException cce) {
				cce.printStackTrace();
			}

		}
	}

}
