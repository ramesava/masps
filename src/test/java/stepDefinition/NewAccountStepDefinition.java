package stepDefinition;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import Pages.USSchoolList;
import Util.WebDriverCommonLib;
import Util.log4jLib;
import Util.webConnect;
import cucumber.api.DataTable;
import cucumber.api.java.en.When;

/**
 * @author Sarvjeet
 */
public class NewAccountStepDefinition extends webConnect {

	WebDriver driver = getDriver();
	Logger log = log4jLib.getLog(NewAccountStepDefinition.class);

	@When("^verify customer number$")
	public void verify_customer_number(DataTable usrData) throws Throwable {

		List<List<String>> data = usrData.raw();

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);

		USSchoolList edReg = PageFactory.initElements(driver, USSchoolList.class);

		edReg.nUsrEmail(data.get(1).get(0));
		edReg.nUsrPwd(data.get(1).get(1));
		edReg.nxtBtn();
		cLib.waitForPageToLoad();
		try {

			if (edReg.bcoe() != null)
				log.info("Customer number is generated for Educator with Email: " + data.get(1).get(0)
						+ " and Customer number: " + edReg.bcoe());

		} catch (Exception e) {
			log.info("Customer number is not generated for Educator with Email: " + data.get(1).get(0));
			e.printStackTrace();
		}

	}

}
