package stepDefinition;

import java.util.List;

//import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import Pages.pwdReset;
import Util.WebDriverCommonLib;
import Util.log4jLib;
import Util.webConnect;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * @author Sarvjeet
 */
public class ForgotPwdPageStepDefinition extends webConnect {

	WebDriver driver = getDriver();
	
//	WebDriver driver = TestBaseProvider.getTestBase().getDriver();
//	Configuration data = TestBaseProvider.getTestBase().getTestData();
	Logger log = log4jLib.getLog(ForgotPwdPageStepDefinition.class);

	@When("^User enters valid email \"([^\"]*)\"$")
	public void user_enters_valid_email(String email) throws Throwable {
		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		pwdReset restPwd = PageFactory.initElements(driver, pwdReset.class);

		cLib.assertDisplayed(restPwd.forgotActLnk());
		restPwd.forgotAcctLnk();
		cLib.assertDisplayed(restPwd.emailWeb());
		restPwd.forgotCancelLnk();
		cLib.assertDisplayed(restPwd.forgotActLnk());
		//AssertUtils.assertDisplayed(restPwd.forgotActLnk());
		restPwd.forgotAcctLnk();
		cLib.assertDisplayed(restPwd.emailWeb());
		restPwd.emailRec(email);
		log.info("User provides email to create new password" );
		restPwd.submitBtn();
		cLib.pause(3000);
	}

	@Then("^confirmation message \"([^\"]*)\" displays$")
	public void confirmation_message_displays(String msg) throws Throwable {
		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		pwdReset restPwd = PageFactory.initElements(driver, pwdReset.class);

		//AssertUtils.assertTextMatches(restPwd.cnfMesg(),Matchers.equalToIgnoringCase(msg));
		cLib.assertTextMatches(restPwd.cnfMesg(),msg);
		log.info("Email has been sent to create new password");
		cLib.waitForElementToBeClickable(restPwd.emailReWeb());
		restPwd.emailRetWeb();
		cLib.pause(3000);
			
	}
  

	@When("^User provides valid name and school details$")
	public void user_provides_valid_name_and_school_details(DataTable formData) throws Throwable {

		List<List<String>> data = formData.raw();
		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		pwdReset restPwd = PageFactory.initElements(driver, pwdReset.class);

			cLib.assertDisplayed(restPwd.forgotActLnk());
			restPwd.forgotAcctLnk();
			cLib.assertDisplayed(restPwd.logInAreaWeb());
			restPwd.retrieveEmailLnk();
			cLib.assertDisplayed(restPwd.emailAreaWeb());
			restPwd.fName(data.get(1).get(0));
			restPwd.lName(data.get(1).get(1));
			restPwd.state(data.get(1).get(2));
			cLib.waitForDisplayed(restPwd.cityWeb());
			restPwd.city(data.get(1).get(3));
			cLib.waitForDisplayed(restPwd.schoolWeb());
			restPwd.schoolSelect(data.get(1).get(4));
			cLib.pause(3000);
			restPwd.forgotBtn();
			cLib.assertDisplayed(restPwd.emailRedWeb());
			log.info("User provides personal details to retrieve email");

	}

	@When("^User provides valid name and zip code$")
	public void user_provides_valid_name_and_zip_code(DataTable formData) throws Throwable {
		List<List<String>> data = formData.raw();

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);

		pwdReset restPwd = PageFactory.initElements(driver, pwdReset.class);

			cLib.assertDisplayed(restPwd.forgotActLnk());	
			restPwd.forgotAcctLnk();
			cLib.assertDisplayed(restPwd.logInAreaWeb());
			restPwd.retrieveEmailLnk();
			cLib.assertDisplayed(restPwd.emailAreaWeb());
			restPwd.fName(data.get(1).get(0));
			restPwd.lName(data.get(1).get(1));
			restPwd.byZipCode(data.get(1).get(2));
			restPwd.schoolByZip();
			cLib.pause(5000);
			restPwd.schoolSelect(data.get(1).get(3));
			cLib.pause(3000);
			restPwd.forgotBtn();
			cLib.assertDisplayed(restPwd.emailRedWeb());
			log.info("User provides personal details to retrieve email");
	}  
	
	
	
    @Then("^user gets their email and choose to login$")
    public void user_gets_their_email_and_choose_to_login() throws Throwable {
    	
		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		pwdReset restPwd = PageFactory.initElements(driver, pwdReset.class);
	
		cLib.assertDisplayed(restPwd.EmailrecWeb());
    	restPwd.EmailRecRadio();
    	cLib.pause(1000);
		restPwd.loginBtn();
		cLib.pause(1000);
		log.info("Email retrieval is successful through personal details");
		restPwd.emailRedirectBtn();
		log.info("User choose to go to login page");
		cLib.pause(3000);
    }

    @And("^User gets the login page$")
    public void user_gets_the_login_page() throws Throwable {
		
    	WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		pwdReset restPwd = PageFactory.initElements(driver, pwdReset.class);
		
		cLib.assertDisplayed(restPwd.loginID());
		log.info("User is on Login Page");
    }
    
    
    
    @Then("^user gets their email and choose to create new password$")
    public void user_gets_their_email_and_choose_to_create_new_password() throws Throwable {
		
    	WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		pwdReset restPwd = PageFactory.initElements(driver, pwdReset.class);
		
		//AssertUtils.assertDisplayed(restPwd.EmailrecWeb());
		cLib.assertDisplayed(restPwd.EmailrecWeb());
		restPwd.EmailRecRadio();
		cLib.pause(1000);
		restPwd.emailBtn();
		cLib.pause(1000);
		log.info("Email retrieval is successful through personal details");
		restPwd.emailRedirectBtn();
    }
    

@Given("^vaalidate the \\[Successful Login\\]$")
public void vaalidate_the_Successful_Login() throws Throwable {
	pwdReset restPwd = PageFactory.initElements(driver, pwdReset.class);
	
	restPwd.forgotAcctLnk();
	//restPwd.forgotCancelLnk();
	// WaitUtils.waitForDisplayed(restPwd.forgotActLnk());
	//restPwd.forgotAcctLnk();
	//restPwd.emailRec(email);
	Thread.sleep(5000);
	//restPwd.emailRec(TestBaseProvider.getTestBase().getTestData().getString("email"));
	Thread.sleep(3000);
	restPwd.submitBtn();
}

@After("@retrievePwd,@retrieveEmail,@retrieveEmailUsingZipCode")
public void embedScreenshot(Scenario scenario) {

	if (scenario.isFailed()) {
		try {
			scenario.write("Current Page URL is " + driver.getCurrentUrl());
			byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
			scenario.embed(screenshot, "image/png");
		} catch (org.openqa.selenium.WebDriverException wde) {
			System.err.println(wde.getMessage());
		} catch (ClassCastException cce) {
			cce.printStackTrace();
		}

	}
}

}
