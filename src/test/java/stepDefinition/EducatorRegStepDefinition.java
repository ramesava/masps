package stepDefinition;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import Pages.USManualSchool;
import Pages.USSchoolList;
import Pages.nonUSManualSchool;
import Util.ScreenShot;
import Util.WebDriverCommonLib;
import Util.log4jLib;
import Util.webConnect;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * @author Sarvjeet
 */
public class EducatorRegStepDefinition extends webConnect {

	WebDriver driver = getDriver();
	Logger log = log4jLib.getLog(EducatorRegStepDefinition.class);

	@Given("^user is on educator registration page$")
	public void user_is_on_educator_registration_page() throws Throwable {
		
		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		USSchoolList edReg = PageFactory.initElements(driver, USSchoolList.class);

		cLib.pause(3000);
		edReg.signUplnk();
		cLib.pause(3000);
		edReg.eduRegLnk();
		cLib.pause(5000);
		

	}

	@When("^user fills valid data in educator personal details page$")
	public void user_fills_valid_data_in_educator_personal_details_page(DataTable formData) throws Throwable {

		List<List<String>> data = formData.raw();

		USSchoolList edReg = PageFactory.initElements(driver, USSchoolList.class);
		edReg.selectTitle(data.get(1).get(0));
		edReg.Fname(data.get(1).get(1));
		edReg.Lname(data.get(1).get(2));
		edReg.email(data.get(1).get(3));
		edReg.cemail(data.get(1).get(4));
		edReg.pwd(data.get(1).get(5));
		edReg.cpwd(data.get(1).get(6));

	}

	@When("^provides school details with his roles and class size$")
	public void provides_school_details_with_his_roles_and_class_size(DataTable formData) throws Throwable {

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);

		List<List<String>> data = formData.raw();

		USSchoolList edReg = PageFactory.initElements(driver, USSchoolList.class);

		log.info("Educator registration started for school location US and school selection from list");
		edReg.clknxt();
		cLib.pause(5000);

		if (edReg.errorSize().size() != 0) {

			if (edReg.errorWeb().isDisplayed()) {
				cLib.waitForPageToLoad();
				log.info(edReg.errorTxt());
				ScreenShot ss = new ScreenShot(driver);
				ss.TestScreenShots("EducatorRegFailedForUS");

			}

		}

		else {
			cLib.waitForPageToLoad();
			edReg.SelectState(data.get(1).get(0));
			//cLib.waitForPageToLoad();
			cLib.pause(5000);
			edReg.SelectCity(data.get(1).get(1));
			//cLib.waitForPageToLoad();
			cLib.pause(5000);
			edReg.SelectSchool();
			edReg.terms();
			edReg.privacy();
			edReg.clknxt();
			edReg.gradechkBx();
			edReg.classSize(data.get(1).get(2));
			edReg.clknxt();
			cLib.pause(5000);
			edReg.clknxt();

		}
	}

	@When("^fills valid data in manual school entry form with roles and class size$")
	public void fills_valid_data_in_manual_school_entry_form_with_roles_and_class_size(DataTable formData)
			throws Throwable {

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);

		List<List<String>> data = formData.raw();

		USManualSchool mSchhol = PageFactory.initElements(driver, USManualSchool.class);
		log.info("Educator registration started for school location US with manual school entry");
		mSchhol.clknxt();
		cLib.pause(5000);

		if (mSchhol.errorSize().size() != 0) {

			if (mSchhol.errorWeb().isDisplayed()) {
				cLib.waitForPageToLoad();;
				log.info(mSchhol.errorTxt());
				ScreenShot ss = new ScreenShot(driver);
				ss.TestScreenShots("EducatorRegFailedForUSManualSchoolEntry");

			}

		}

		else {
			cLib.waitForPageToLoad();
			mSchhol.SelectState(data.get(1).get(0));
			cLib.waitForPageToLoad();
			mSchhol.SelectCity(data.get(1).get(1));
			cLib.pause(5000);
			mSchhol.manualSLnk();
			cLib.pause(5000);
			mSchhol.Sname(data.get(1).get(2));
			mSchhol.schoolType(data.get(1).get(3));
			mSchhol.AddLine1(data.get(1).get(4));
			mSchhol.city(data.get(1).get(5));
			mSchhol.state(data.get(1).get(0));
			mSchhol.zipCode(data.get(1).get(6));
			mSchhol.phone(data.get(1).get(7));
			mSchhol.terms();
			mSchhol.privacy();
			mSchhol.clknxt();
			mSchhol.gradechkBx();
			mSchhol.classSize(data.get(1).get(8));
			mSchhol.clknxt();
			cLib.pause(5000);
			mSchhol.clknxt();

		}
	}

	@When("^Select home school option and fills manual school entry form with roles and class size$")
	public void select_home_school_option_and_fills_manual_school_entry_form_with_roles_and_class_size(
			DataTable formData) throws Throwable {

		List<List<String>> data = formData.raw();
		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		USManualSchool mSchhol = PageFactory.initElements(driver, USManualSchool.class);

		log.info("Educator registration started for school location home school");
		mSchhol.schoolLoc();
		mSchhol.clknxt();
		cLib.waitForPageToLoad();
		cLib.pause(2000);

		if (mSchhol.errorSize().size() != 0) {

			if (mSchhol.errorWeb().isDisplayed()) {
				cLib.waitForPageToLoad();
				log.info(mSchhol.errorTxt());
				ScreenShot ss = new ScreenShot(driver);
				ss.TestScreenShots("EducatorRegFailedForHomeSchool");

			}

		}

		else {
			cLib.waitForPageToLoad();
			mSchhol.Sname(data.get(1).get(0));
			mSchhol.AddLine1(data.get(1).get(1));
			mSchhol.city(data.get(1).get(2));
			mSchhol.state(data.get(1).get(3));
			mSchhol.zipCode(data.get(1).get(4));
			mSchhol.phone(data.get(1).get(5));
			mSchhol.terms();
			mSchhol.privacy();
			mSchhol.clknxt();
			mSchhol.gradechkBxx();
			mSchhol.clknxt();
			cLib.pause(5000);
			mSchhol.clknxt();

		}
	}

	@When("^Select non US school option and fills manual school entry form with roles and class size$")
	public void select_non_US_school_option_and_fills_manual_school_entry_form_with_roles_and_class_size(
			DataTable formData) throws Throwable {

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);

		List<List<String>> data = formData.raw();

		nonUSManualSchool nUsSchool = PageFactory.initElements(driver, nonUSManualSchool.class);

		log.info("Educator registration started for school location Non US school");
		nUsSchool.schoolLoc();
		nUsSchool.clknxt();
		cLib.pause(4000);

		if (nUsSchool.errorSize().size() != 0) {

			if (nUsSchool.errorWeb().isDisplayed()) {
				cLib.waitForPageToLoad();
				log.info(nUsSchool.errorTxt());
				ScreenShot ss = new ScreenShot(driver);
				ss.TestScreenShots("EducatorRegFailedForNonUS");

			}

		}

		else {
			cLib.waitForPageToLoad();
			nUsSchool.country(data.get(1).get(0));
			cLib.pause(7000);
			nUsSchool.country(data.get(1).get(9));
			cLib.pause(7000);
			nUsSchool.manualSLnk();
			cLib.pause(3000);
			nUsSchool.Sname(data.get(1).get(1));
			nUsSchool.schoolType(data.get(1).get(2));
			nUsSchool.AddLine1(data.get(1).get(3));
			nUsSchool.city(data.get(1).get(4));
			nUsSchool.state(data.get(1).get(5));
			nUsSchool.zipCode(data.get(1).get(6));
			nUsSchool.SelectCntry(data.get(1).get(0));
			nUsSchool.phone(data.get(1).get(7));
			nUsSchool.terms();
			nUsSchool.privacy();
			nUsSchool.clknxt();
			nUsSchool.gradechkBx();
			nUsSchool.classSize(data.get(1).get(8));
			nUsSchool.clknxt();
			cLib.pause(5000);
			nUsSchool.clknxt();

		}
	}

	@When("^select school based on zip code from the list and provide details about roles and class size$")
	public void select_school_based_on_zip_code_from_the_list_and_provide_details_about_roles_and_class_size(
			DataTable formData) throws Throwable {

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);

		List<List<String>> data = formData.raw();

		USSchoolList edReg = PageFactory.initElements(driver, USSchoolList.class);
		log.info("Educator registration started for school location Military Base");
		edReg.militaryBase();
		edReg.clknxt();
		cLib.pause(3000);

		if (edReg.errorSize().size() != 0) {

			if (edReg.errorWeb().isDisplayed()) {
				cLib.waitForPageToLoad();
				log.info(edReg.errorTxt());
				ScreenShot ss = new ScreenShot(driver);
				ss.TestScreenShots("EducatorRegFailedForMilitaryBase");

			}

		}

		else {
			cLib.waitForPageToLoad();
			edReg.zipCode(data.get(1).get(0));
			edReg.findSchoolBtn();
			cLib.waitForPageToLoad();
			edReg.SelectSchool();
			edReg.terms();
			edReg.privacy();
			edReg.clknxt();
			edReg.gradechkBx();
			edReg.classSize(data.get(1).get(1));
			edReg.clknxt();
			cLib.pause(5000);
			edReg.clknxt();
		}
	}

	@Then("^Verify the teacher Registration confirmation page$")
	public void verify_the_teacher_Registration_confirmation_page(DataTable formData) throws Throwable {

		List<List<String>> data = formData.raw();
		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		USSchoolList edReg = PageFactory.initElements(driver, USSchoolList.class);

		cLib.waitForPageToLoad();
		cLib.assertTextMatches(edReg.cnfWeb(), data.get(1).get(0));
		log.info("Educator registration Completed");

	}

	@After("@EduRegUSSchool,@EduRegManualSchoolEntry,@EduRegMilitaryBase,@EduRegHomeSchool,@EduRegNonUSSchool")
	public void embedScreenshot(Scenario scenario) {

		if (scenario.isFailed()) {
			try {
				scenario.write("Current Page URL is " + driver.getCurrentUrl());
				byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
				scenario.embed(screenshot, "image/png");
			} catch (org.openqa.selenium.WebDriverException wde) {
				System.err.println(wde.getMessage());
			} catch (ClassCastException cce) {
				cce.printStackTrace();
			}

		}
	}

}