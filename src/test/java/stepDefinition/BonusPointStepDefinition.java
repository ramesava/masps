package stepDefinition;

import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import Pages.BonusPoint;
import Pages.USSchoolList;
import Pages.pwdChange;
import Util.WebDriverCommonLib;
import Util.log4jLib;
import Util.webConnect;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * @author Sarvjeet
 */
public class BonusPointStepDefinition extends webConnect {

	WebDriver driver = getDriver();
	Logger log = log4jLib.getLog(BonusPointStepDefinition.class);

	@Given("^user is loged in with valid email \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void user_is_loged_in_with_valid_email_and_password(String email, String pwd) throws Throwable {

		pwdChange pwdCnge = PageFactory.initElements(driver, pwdChange.class);
		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		USSchoolList edReg = PageFactory.initElements(driver, USSchoolList.class);

		pwdCnge.loginId(email);
		pwdCnge.LoginPwd(pwd);
		pwdCnge.nxtBtn();
		cLib.assertDisplayed(edReg.nameWel());

	}

	@When("^user goes to bonus point page$")
	public void user_goes_to_bonus_point_page() throws Throwable {

		BonusPoint bonus = PageFactory.initElements(driver, BonusPoint.class);
		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);

		bonus.bonusTab();
		cLib.pause(3000);

	}

	@Then("^user checks their bonus point balance$")
	public void user_checks_their_bonus_point_balance() throws Throwable {

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		BonusPoint bonus = PageFactory.initElements(driver, BonusPoint.class);

			cLib.assertDisplayed(bonus.bonusWeb());
			log.info("Bonus Point Balance: " + bonus.bonusPt());
			log.info("Bonus Point Earned this Year: " + bonus.thisYrPt());
			logout();

			
	}

	@After("@BP")
	public void embedScreenshot(Scenario scenario) {

		if (scenario.isFailed()) {
			try {
				scenario.write("Current Page URL is " + driver.getCurrentUrl());
				byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
				scenario.embed(screenshot, "image/png");
			} catch (org.openqa.selenium.WebDriverException wde) {
				System.err.println(wde.getMessage());
			} catch (ClassCastException cce) {
				cce.printStackTrace();
			}

		}
	}

}
