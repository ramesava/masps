package stepDefinition;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import Pages.USSchoolList;
import Util.ScreenShot;
import Util.WebDriverCommonLib;
import Util.log4jLib;
import Util.webConnect;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * @author Sarvjeet Kumar Singh
 */
public class ParentRegStepDefinition extends webConnect {

	WebDriver driver = getDriver();
	Logger log = log4jLib.getLog(ParentRegStepDefinition.class);

	@Given("^user is on parent registration page$")
	public void user_is_on_parent_registration_page() throws Throwable {

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		USSchoolList edReg = PageFactory.initElements(driver, USSchoolList.class);

		cLib.waitForLoaderToDismiss();
		edReg.signUplnk();
		cLib.waitForLoaderToDismiss();
		edReg.parentRegLnk();
		cLib.pause(5000);

	}

	@When("^user fills valid data in the parent registration form$")
	public void fills_valid_data_in_the_parent_registration_form(DataTable formData) throws Throwable {

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);

		List<List<String>> data = formData.raw();

		USSchoolList edReg = PageFactory.initElements(driver, USSchoolList.class);

		log.info("Registration started for Parent");
		
		edReg.selectTitle(data.get(1).get(0));
		edReg.Fname(data.get(1).get(1));
		edReg.Lname(data.get(1).get(2));
		edReg.email(data.get(1).get(3));
		edReg.cemail(data.get(1).get(4));
		edReg.pwd(data.get(1).get(5));
		edReg.cpwd(data.get(1).get(6));
		edReg.terms();
		edReg.privacy();
		edReg.clknxt();
		cLib.pause(2000);

		if (edReg.errorSize().size() != 0) {

			if (edReg.errorWeb().isDisplayed()) {
				log.info(edReg.errorTxt());
				ScreenShot ss = new ScreenShot(driver);
				ss.TestScreenShots("ParentRegFailed");

			}

		}

		else {

			cLib.waitForDisplayed(edReg.dspSvBtn());
			edReg.cFname(data.get(1).get(7));
			edReg.cLname(data.get(1).get(8));
			edReg.SelectGrade();
			edReg.gender();
			edReg.SelectMonth();
			edReg.SelectYear();
			edReg.svBtn();
			cLib.waitForDisplayed(edReg.skipBtn());
			edReg.parentSkipBtn();
		}

	}

	@Then("^Verify the parent Registration confirmation page$")
	public void verify_the_parent_teacher_Registration_confirmation_page(DataTable formData) throws Throwable {

		List<List<String>> data = formData.raw();
		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		USSchoolList edReg = PageFactory.initElements(driver, USSchoolList.class);

		cLib.pause(2000);
		cLib.assertTextMatches(edReg.parentCnfmMsgWeb(), data.get(1).get(0));
		log.info("Registration Completed for Parent");

	}

	@After("@ParentReg")
	public void embedScreenshot(Scenario scenario) {

		if (scenario.isFailed()) {
			try {
				scenario.write("Current Page URL is " + driver.getCurrentUrl());
				byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
				scenario.embed(screenshot, "image/png");
			} catch (org.openqa.selenium.WebDriverException wde) {
				System.err.println(wde.getMessage());
			} catch (ClassCastException cce) {
				cce.printStackTrace();
			}

		}
	}

}