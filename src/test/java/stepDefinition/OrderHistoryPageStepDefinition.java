package stepDefinition;

import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import Pages.OrderHistory;
import Pages.USSchoolList;
import Pages.pwdChange;
import Util.WebDriverCommonLib;
import Util.log4jLib;
import Util.webConnect;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * @author Sarvjeet
 */
public class OrderHistoryPageStepDefinition extends webConnect {

	WebDriver driver = getDriver();
	Logger log = log4jLib.getLog(OrderHistoryPageStepDefinition.class);

	@Given("^user login with valid email \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void user_login_with_valid_email_and_password(String email, String pwd) throws Throwable {

		pwdChange pwdCnge = PageFactory.initElements(driver, pwdChange.class);
		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		USSchoolList edReg = PageFactory.initElements(driver, USSchoolList.class);

		pwdCnge.loginId(email);
		pwdCnge.LoginPwd(pwd);
		pwdCnge.nxtBtn();
		cLib.assertDisplayed(edReg.nameWel());

	}

	@Given("^user is on order history page$")
	public void user_is_on_order_history_page() throws Throwable {

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		OrderHistory orderH = PageFactory.initElements(driver, OrderHistory.class);

		orderH.orderLnk();
		cLib.assertDisplayed(orderH.orderWeb());
		cLib.pause(2000);

	}

	@When("^user go to view order details page$")
	public void user_go_to_view_order_details_page() throws Throwable {

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		OrderHistory orderH = PageFactory.initElements(driver, OrderHistory.class);

		orderH.viewOrderLnk();
		cLib.assertDisplayed(orderH.orderDate());
		cLib.pause(5000);
		log.info("User is on view order detail page");

	}

	@Then("^returns to order history page$")
	public void returns_to_order_history_page() throws Throwable {

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		OrderHistory orderH = PageFactory.initElements(driver, OrderHistory.class);

		orderH.returnOrderHLnk();
		cLib.assertDisplayed(orderH.orderWeb());
		cLib.pause(2000);
		logout();
	}

	@When("^user go to order number page$")
	public void user_go_to_order_number_page() throws Throwable {

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		OrderHistory orderH = PageFactory.initElements(driver, OrderHistory.class);

		orderH.orderNoLnk();
		cLib.assertDisplayed(orderH.orderWebE());
		log.info("User is on order number page");
	}

	@Then("^user verifies their order$")
	public void user_verifies_their_order() throws Throwable {

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		OrderHistory orderH = PageFactory.initElements(driver, OrderHistory.class);

		cLib.pause(5000);
		log.info(orderH.orderDate().getText());
	}
	
	
	@After("@viewOrderDetail,@trackOrder")
	public void embedScreenshot(Scenario scenario) {

		if (scenario.isFailed()) {
			try {
				scenario.write("Current Page URL is " + driver.getCurrentUrl());
				byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
				scenario.embed(screenshot, "image/png");
			} catch (org.openqa.selenium.WebDriverException wde) {
				System.err.println(wde.getMessage());
			} catch (ClassCastException cce) {
				cce.printStackTrace();
			}

		}
		
	}

}
