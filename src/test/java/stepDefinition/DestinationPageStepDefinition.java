package stepDefinition;

import java.net.URL;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import Pages.USSchoolList;
import Pages.destination;
import Pages.pwdChange;
import Util.WebDriverCommonLib;
import Util.brokenLink;
import Util.log4jLib;
import Util.webConnect;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

/**
 * @author Sarvjeet
 */
public class DestinationPageStepDefinition extends webConnect {

	WebDriver driver = getDriver();
	Logger log = log4jLib.getLog(DestinationPageStepDefinition.class);

	@Given("^user logIn with valid credentials$")
	public void user_logIn_with_valid_credentials(DataTable usercredentials) throws Throwable {
		List<List<String>> data = usercredentials.raw();
		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		pwdChange pwdCnge = PageFactory.initElements(driver, pwdChange.class);
		USSchoolList edReg = PageFactory.initElements(driver, USSchoolList.class);

		pwdCnge.loginId(data.get(1).get(0));
		pwdCnge.LoginPwd(data.get(1).get(1));
		pwdCnge.nxtBtn();
		cLib.assertDisplayed(edReg.nameWel());
	}

	@When("^verify the destination link$")
	public void verify_the_destination_link(List<String> expected) throws Throwable {

		brokenLink bLink = new brokenLink();
		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		destination des = PageFactory.initElements(driver, destination.class);

		cLib.waitForPageToLoad();
		des.desLnk();
		
		Iterator<String> inIterator = expected.iterator();

		log.info("Expected popular links for Educator are: ");

		while (inIterator.hasNext()) {
			String row = inIterator.next();
			log.info(row);
		}

		log.info("Actual popular links for Educator are: ");

		if (des.destLnk().size() > 0) {

			for (WebElement st : des.destLnk()) {
				String actual = st.getAttribute("href");
				log.info(actual);
			}

			log.info("Popular links for educator are verified and below are their status code returned from the server.");
			for (WebElement element : des.destLnk()) {

				try

				{

					log.info("URL: " + element.getAttribute("href") + " returned "
							+ bLink.isLinkBroken(new URL(element.getAttribute("href"))));

				}

				catch (Exception exp)

				{

					log.info("At " + element.getAttribute("innerHTML") + " Exception occured -&gt; " + exp.getMessage());
					exp.printStackTrace();

				}

			}
		}

		else {
			log.info("Links are not present");
		}

		logout();
	}
	
	@After("@VerifyDestinationLink")
	public void embedScreenshot(Scenario scenario) {

		if (scenario.isFailed()) {
			try {
				scenario.write("Current Page URL is " + driver.getCurrentUrl());
				byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
				scenario.embed(screenshot, "image/png");
			} catch (org.openqa.selenium.WebDriverException wde) {
				System.err.println(wde.getMessage());
			} catch (ClassCastException cce) {
				cce.printStackTrace();
			}

		}
	}

}
