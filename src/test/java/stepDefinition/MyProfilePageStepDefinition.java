package stepDefinition;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import Pages.addChild;
import Pages.emailEdit;
import Pages.myAccount;
import Pages.nameChange;
import Pages.profileQEdit;
import Pages.pwdChange;
import Pages.roleEdit;
import Util.WebDriverCommonLib;
import Util.log4jLib;
import Util.webConnect;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * @author Sarvjeet
 */
public class MyProfilePageStepDefinition extends webConnect {

	WebDriver driver = getDriver();
	Logger log = log4jLib.getLog(MyProfilePageStepDefinition.class);

	@Given("^user log in with valid credential$")
	public void user_log_in_with_valid_credential(DataTable usercredentials) throws Throwable {
		List<List<String>> data = usercredentials.raw();
		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		pwdChange pwdCnge = PageFactory.initElements(driver, pwdChange.class);

		pwdCnge.loginId(data.get(1).get(0));
		pwdCnge.LoginPwd(data.get(1).get(1));
		pwdCnge.nxtBtn();
		cLib.pause(5000);

	}

	@When("^user changes his password$")
	public void user_changes_his_password(DataTable usercredentials) throws Throwable {
		List<List<String>> data = usercredentials.raw();

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);

		pwdChange pwdCnge = PageFactory.initElements(driver, pwdChange.class);

		cLib.waitForPageToLoad();
		pwdCnge.pChangeLnk();
		pwdCnge.cancelBtn();
		cLib.assertDisplayed(pwdCnge.changePwd());
		pwdCnge.pChangeLnk();
		pwdCnge.cPwd(data.get(1).get(0));
		pwdCnge.nPwd(data.get(1).get(1));
		pwdCnge.ncPwd(data.get(1).get(1));
		pwdCnge.cBtn();
	}

	@When("^user changes his Name$")
	public void user_changes_his_Name(DataTable nameChange) throws Throwable {

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);

		List<List<String>> data = nameChange.raw();

		nameChange nameChnge = PageFactory.initElements(driver, nameChange.class);

		cLib.waitForPageToLoad();
		nameChnge.nChange();
		nameChnge.cancelBtn();
		cLib.assertDisplayed(nameChnge.fnameProfile());
		nameChnge.nChange();
		nameChnge.changeBtn();
		nameChnge.cBtn();
		nameChnge.titleDropDown(data.get(1).get(0));
		nameChnge.firstName(data.get(1).get(1));
		nameChnge.lastName(data.get(1).get(2));
		nameChnge.saveBtn();
	}

	@When("^user edits his email$")
	public void user_edits_his_email(DataTable formData) throws Throwable {

		List<List<String>> data = formData.raw();

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);

		emailEdit emailEdt = PageFactory.initElements(driver, emailEdit.class);

		cLib.waitForPageToLoad();
		emailEdt.emailChangeLnk();
		emailEdt.cancelBtn();
		cLib.assertDisplayed(emailEdt.emailProfile());
		emailEdt.emailChangeLnk();
		emailEdt.newEmail(data.get(1).get(0));
		emailEdt.cnfEmail(data.get(1).get(1));
		emailEdt.cntBtn();
	}

	@When("^user edits role and class size as \"([^\"]*)\"$")
	public void user_edits_role_and_class_size_as(String cSize) throws Throwable {

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);

		roleEdit roleEdt = PageFactory.initElements(driver, roleEdit.class);

		cLib.waitForPageToLoad();
		roleEdt.roleEdtLnk();
		roleEdt.cancelBtn();
		cLib.assertDisplayed(roleEdt.roleTxt());
		roleEdt.roleEdtLnk();
		roleEdt.reset();
		roleEdt.grade();
		roleEdt.classSize(cSize);
		roleEdt.continueBtn();
	}

	@When("^user edits the profile questions$")
	public void user_edits_the_profile_questions() throws Throwable {

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);

		profileQEdit profileEdt = PageFactory.initElements(driver, profileQEdit.class);

		cLib.pause(3000);
		profileEdt.profileQELnk();
		cLib.waitForPageToLoad();
		profileEdt.cancelBtn();
		cLib.assertDisplayed(profileEdt.profileQ());
		profileEdt.profileQELnk();
		profileEdt.readingLvlSel();
		profileEdt.teacherBtn();
		profileEdt.mentorBtn();
		profileEdt.bookBtn();
		profileEdt.BMonthSel();
		profileEdt.BDaySel();
		profileEdt.contineBtn();
	}

	@When("^user enters the child details$")
	public void user_enters_the_child_details(DataTable addchild) throws Throwable {

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);

		List<List<String>> data = addchild.raw();

		addChild childDetail = PageFactory.initElements(driver, addChild.class);

		cLib.waitForPageToLoad();
		childDetail.fName(data.get(1).get(0));
		childDetail.lName(data.get(1).get(1));
		childDetail.grade(data.get(1).get(2));
		childDetail.gender();
		childDetail.monthDropDown(data.get(1).get(3));
		childDetail.yearDropDown(data.get(1).get(4));
		childDetail.CAC(data.get(1).get(5));
		childDetail.submitBtn();
		cLib.waitForPageToLoad();
		childDetail.savevBtn();
	}
	
	
	@When("^user edits the child details$")
	public void user_edits_the_child_details(DataTable editChild) throws Throwable {

    	WebDriverCommonLib cLib = new WebDriverCommonLib(driver);

		List<List<String>> data = editChild.raw();

		addChild childDetail = PageFactory.initElements(driver, addChild.class);

		cLib.waitForPageToLoad();
		childDetail.parentExBtn();
		cLib.pause(2000);
		childDetail.childEditBtn();
		cLib.pause(3000);
		childDetail.childCleartBtn();
		cLib.pause(3000);
		childDetail.fName(data.get(1).get(0));
		childDetail.lName(data.get(1).get(1));
		childDetail.grade(data.get(1).get(2));
		childDetail.gender();
		childDetail.monthDropDown(data.get(1).get(3));
		childDetail.yearDropDown(data.get(1).get(4));
		childDetail.CAC(data.get(1).get(5));
		childDetail.submitBtn();
		cLib.waitForPageToLoad();
		childDetail.savevBtn();
    }
    
    @When("^user delete the child details$")
    public void user_delete_the_child_details() throws Throwable {
       	
    	WebDriverCommonLib cLib = new WebDriverCommonLib(driver);

    	addChild childDetail = PageFactory.initElements(driver, addChild.class);

    		cLib.waitForPageToLoad();
    		childDetail.parentExBtn();
    		cLib.pause(2000);
    		childDetail.deleteChildBtn();
    		cLib.pause(3000);
    		cLib.alertAccept();
    } 

	@When("^user verifies myAccount links$")
	public void user_verifies_myAccount_links(List<String> expected) throws Throwable {

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);

		myAccount myAcct = PageFactory.initElements(driver, myAccount.class);

		log.info(" EXP TABS ARE " + expected + "  -- " + expected.size());

		Iterator<String> inIterator = expected.iterator();
		while (inIterator.hasNext()) {
			String row = inIterator.next();
			log.info("EXPECTED TAB IS :" + row);

			try {
				cLib.waitForPageToLoad();
				String actual = myAcct.getByName(row, "click");
				log.info("TEXT UNDER THIS TAB IS :" + actual);
				// Assert.assertEquals(row, actual);

			} catch (Exception e) {
				log.info(e.getMessage());
				e.printStackTrace();

			}

		}
		log.info("All the link verified successfully");
		logout();
	}
	
    @Then("^user should comes back to my profile page$")
    public void user_should_comes_back_to_my_profile_page() throws Throwable {
    	
    	WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
    	nameChange nameChnge = PageFactory.initElements(driver, nameChange.class);
    	
    	cLib.assertDisplayed(nameChnge.fnameProfile());
    	logout();
    }
	
	
	@After("@pwdEdit,@nameEdit,@emailEdit,@roleEdit,@profileQEdit,@addChild,@editChild,@deleteChild,@verifyLink")
	public void embedScreenshot(Scenario scenario) {

		if (scenario.isFailed()) {
			try {
				scenario.write("Current Page URL is " + driver.getCurrentUrl());
				byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
				scenario.embed(screenshot, "image/png");
			} catch (org.openqa.selenium.WebDriverException wde) {
				System.err.println(wde.getMessage());
			} catch (ClassCastException cce) {
				cce.printStackTrace();
			}

		}
	}
	
	

}
