package stepDefinition;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import Pages.USSchoolList;
import Pages.addAddress;
import Pages.pwdChange;
import Util.WebDriverCommonLib;
import Util.log4jLib;
import Util.webConnect;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * @author Sarvjeet
 */
public class AddressPageStepDefinition extends webConnect {

	WebDriver driver = getDriver();
	Logger log = log4jLib.getLog(AddressPageStepDefinition.class);

	@Given("^user login with valid credentials$")
	public void user_login_with_valid_credentials(DataTable usercredentials) throws Throwable {
		List<List<String>> data = usercredentials.raw();
		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);
		pwdChange pwdCnge = PageFactory.initElements(driver, pwdChange.class);
		USSchoolList edReg = PageFactory.initElements(driver, USSchoolList.class);

		pwdCnge.loginId(data.get(1).get(0));
		pwdCnge.LoginPwd(data.get(1).get(1));
		pwdCnge.nxtBtn();
		cLib.assertDisplayed(edReg.nameWel());

	}

	@When("^user edits the school address$")
	public void user_edits_the_school_address(DataTable editadress) throws Throwable {

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);

		List<List<String>> data = editadress.raw();

		addAddress addr = PageFactory.initElements(driver, addAddress.class);

		cLib.waitForPageToLoad();
		addr.addrTab();
		addr.editLink();
		addr.changeAdd();
		addr.continueBtn();
		addr.schoolName(data.get(1).get(0));
		addr.adrLine1(data.get(1).get(1));
		addr.adrLine2(data.get(1).get(2));
		addr.postalCode(data.get(1).get(3));
		addr.ctyState();
		cLib.pause(3000);
		addr.svBtn();
		addr.continueBtn();
	}

	@When("^user edits the address$")
	public void user_edits_the_address(DataTable editadress) throws Throwable {

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);

		List<List<String>> data = editadress.raw();

		addAddress addr = PageFactory.initElements(driver, addAddress.class);

		cLib.waitForPageToLoad();
		addr.addrTab();
		addr.editAddrLnk();
		addr.cancelBtn();
		cLib.waitForPageToLoad();
		addr.editAddrLnk();
		addr.description(data.get(1).get(0));
		addr.fName(data.get(1).get(1));
		addr.lName(data.get(1).get(2));
		addr.adrLine1(data.get(1).get(3));
		addr.adrLine2(data.get(1).get(4));
		addr.postalCode(data.get(1).get(5));
		addr.ctyState();
		cLib.waitForPageToLoad();
		addr.svBtn();
	}

	@When("^user deletes the first address$")
	public void user_deletes_the_first_address() throws Throwable {

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);

		addAddress addr = PageFactory.initElements(driver, addAddress.class);

		cLib.waitForPageToLoad();
		addr.addrTab();
		addr.deleteBtn();
	}

	@When("^user adds the new address$")
	public void user_adds_the_new_address(DataTable address) throws Throwable {

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);

		List<List<String>> data = address.raw();

		addAddress addr = PageFactory.initElements(driver, addAddress.class);

		cLib.waitForPageToLoad();
		addr.addrTab();
		addr.addAddr();
		addr.description(data.get(1).get(0));
		addr.fName(data.get(1).get(1));
		addr.lName(data.get(1).get(2));
		addr.adrLine1(data.get(1).get(3));
		addr.adrLine2(data.get(1).get(4));
		addr.postalCode(data.get(1).get(5));
		addr.ctyState();
		cLib.pause(3000);
		cLib.waitForPageToLoad();
		addr.svBtn();
}
	
	@Then("^user should comes back to address page$")
	public void user_should_comes_back_to_address_page() throws Throwable {

		WebDriverCommonLib cLib = new WebDriverCommonLib(driver);

		addAddress addr = PageFactory.initElements(driver, addAddress.class);
    	cLib.assertDisplayed(addr.addressOrg());
    	logout();
    }
		
	
	@After("@editSchoolAddress,@addAddress,@editAddress,@deleteAddress")
	public void embedScreenshot(Scenario scenario) {

		if (scenario.isFailed()) {
			try {
				scenario.write("Current Page URL is " + driver.getCurrentUrl());
				byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
				scenario.embed(screenshot, "image/png");
			} catch (org.openqa.selenium.WebDriverException wde) {
				System.err.println(wde.getMessage());
			} catch (ClassCastException cce) {
				cce.printStackTrace();
			}

		}
	}

}
