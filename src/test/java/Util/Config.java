/**
 * 
 */
package Util;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

/**
 * @author RLE0293
 *
 */
public class Config extends webConnect {

	Logger log = log4jLib.getLog(Config.class);

	@BeforeTest
	public void setUp() {

		log.info("Test started");
	}

	@BeforeSuite
	public void cleanUp() {
		try {
			FileUtils.deleteDirectory(new File(System.getProperty("user.dir") + "/screenshot"));
		} catch (IOException e) {
			log.error("Error deleting the screenshot directory");
			e.printStackTrace();
		}
	}

	@AfterTest
	public void tearDown() {

		log.info("Test ended");
	}

	@AfterSuite
	public void closeUp() {
		closeDriver();
	}

	/*
	 * @After() public void embedScreenshot(Scenario scenario) {
	 * 
	 * if (scenario.isFailed()) { try { byte[] screenshot = ((TakesScreenshot)
	 * driver) .getScreenshotAs(OutputType.BYTES); scenario.embed(screenshot,
	 * "image/png"); } catch (org.openqa.selenium.WebDriverException wde) {
	 * System.err.println(wde.getMessage()); } catch (ClassCastException cce) {
	 * cce.printStackTrace(); }
	 * 
	 * } }
	 */

}
