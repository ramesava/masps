/**
 * 
 */
package Util;

import org.openqa.selenium.WebElement;

/**
 * @author RLE0293
 *
 */

public abstract interface extendedElement extends WebElement {
	public abstract boolean isPresent();
}
