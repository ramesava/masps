package Util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class webConnect {

	protected static WebDriver driver;
	static String chromeDriverPath = "/chrome/win/chromedriver.exe";
	static String IEdriverPath = "/IE/IEDriverServer.exe";
	static String LinuxdriverPath = "/chrome/linux/chromedriver";

	static String prop = "/config.properties";
	static Properties OR;

	static String getOS = System.getProperty("os.name");
	static Boolean LinuxOS = getOS.toLowerCase().contains("linux");

	protected WebDriver getDriver() {

		String Path_OR = System.getProperty("user.dir") + prop;
		FileInputStream fs = null;
		try {
			fs = new FileInputStream(Path_OR);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		OR = new Properties(System.getProperties());
		try {
			OR.load(fs);
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (driver == null) {

			if (OR.getProperty("browser").equalsIgnoreCase("firefox")) {

				if (LinuxOS) {
					System.out.println("OS: "+System.getProperty("os.name") + "  "+ System.getProperty("os.arch"));
					driver = new FirefoxDriver();
					//driver=new PhantomJSDriver(null);
				} else {
					System.out.println("OS: "+System.getProperty("os.name") + "  "+ System.getProperty("os.arch"));
					driver = new FirefoxDriver();
				}
			} else if (OR.getProperty("browser").equalsIgnoreCase("chrome")) {

				if (LinuxOS) {
					System.out.println("OS: "+System.getProperty("os.name") + "  "+ System.getProperty("os.arch"));
					System.out.println("Chrome Driver Path: "+ System.getProperty("user.dir")+ LinuxdriverPath);
					System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + LinuxdriverPath);
					DesiredCapabilities capabilities = DesiredCapabilities.chrome();
					ChromeOptions options = new ChromeOptions();
					options.addArguments("test-type");
					options.addArguments("no-sandbox");
					options.addArguments("ignore-certificate-errors");
					options.addArguments("--disable-extensions");
					options.addArguments("--disable-web-security");
					options.addArguments("--allow-running-insecure-content");
					capabilities.setCapability("chrome.binary", LinuxdriverPath);
					capabilities.setCapability(ChromeOptions.CAPABILITY, options);

					driver = new ChromeDriver(capabilities);
				} else {
					System.out.println("OS: "+System.getProperty("os.name") + "  "+ System.getProperty("os.arch"));
					System.out.println("Chrome Driver Path: " + System.getProperty("user.dir")+ chromeDriverPath);
					System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + chromeDriverPath);
					DesiredCapabilities capabilities = DesiredCapabilities.chrome();
					ChromeOptions options = new ChromeOptions();
					options.addArguments("test-type");
					options.addArguments("no-sandbox");
					options.addArguments("--disable-extensions");
					options.addArguments("ignore-certificate-errors"); 
					options.addArguments("--allow-running-insecure-content");
					capabilities.setCapability("chrome.binary", chromeDriverPath);
					capabilities.setCapability(ChromeOptions.CAPABILITY, options);
					driver = new ChromeDriver(capabilities);

				}

			}

			else if (OR.getProperty("browser").equalsIgnoreCase("ie")) {
				System.out.println(System.getProperty("os.name")+ "  "+ System.getProperty("os.arch"));
				System.out.println("IE Driver Path: " + System.getProperty("user.dir")+ IEdriverPath);
				System.setProperty("webdriver.ie.driver", System.getProperty("user.dir") + IEdriverPath);
				driver = new InternetExplorerDriver();
			}
		}

		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.get(OR.getProperty("url"));
		return driver;
	}

	public void closeDriver() {
		driver.close();
		driver.quit();
	}

	public void logout() {
		driver.get(OR.getProperty("logout"));
	}
}