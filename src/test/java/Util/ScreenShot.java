/**
 * 
 */
package Util;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

/**
 * @author RLE0293
 *
 */
public class ScreenShot {

	WebDriver driver;

	public ScreenShot(WebDriver driver) {

		this.driver = driver;
	}

	public void TestScreenShots(String Testcase) throws IOException {

//		String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());
		String pattern = "yyyy-MM-dd-HH-mm-ss";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

		String date = simpleDateFormat.format(new Date());
		String path = null;

		EventFiringWebDriver edriver = new EventFiringWebDriver(driver);
		File srcFile = edriver.getScreenshotAs(OutputType.FILE);
		path = System.getProperty("user.dir") + "/screenshot/" + Testcase + "/" + date + ".png";
		File desFile = new File(path);
		FileUtils.copyFile(srcFile, desFile);

	}

}
