package Util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.google.common.base.Function;

/**
 * @author Sarvjeet Singh
 *
 */

public class WebDriverCommonLib {

	static WebDriver driver;
	Logger log = log4jLib.getLog(WebDriverCommonLib.class);

	public WebDriverCommonLib(WebDriver driver) {

		WebDriverCommonLib.driver = driver;

	}

	// To wait for page to load function
	public void waitForPageToLoad() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	

	// To wait for particular link to load function
	public void waitForLinkPresent(String linkName) {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText(linkName)));
	}

	// To wait till the xpath get loaded
	public void waitForXpathPresent(String wbXpath) {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(wbXpath)));
	}

	public void pause(long ms) {
		try {
			Thread.sleep(ms);
		} catch (Exception e) {

		}
	}

	public void alertAccept() {
		Alert alt = driver.switchTo().alert();
		alt.accept();
		// System.out.println(alt.getText());
	}

	public void alertDismiss() {
		Alert alt = driver.switchTo().alert();
		alt.accept();
		// System.out.println(alt.getText());
	}

	// Waiting 30 seconds for an element to be present on the page, checking for its presence once every 5 milliseconds.

	/**
	 * @param element
	 */
	public void waitForDisplayed(WebElement element) {

		Wait<WebElement> wait = new FluentWait<WebElement>(element)

		.withTimeout(30, TimeUnit.SECONDS)

		.pollingEvery(5, TimeUnit.SECONDS)

		.ignoring(NoSuchElementException.class);

		wait.until(new Function<WebElement, Boolean>() {

			public Boolean apply(WebElement element) {

				try {
					return Boolean.valueOf(element.isDisplayed());
				} catch (StaleElementReferenceException e) {
				}
				return null;
			}
		});
	}

	/**
	 * @param element
	 */
	public void assertDisplayed(WebElement element) throws Exception{
		try {
			waitForDisplayed(element);
		} catch (Exception localException) {
			log.error("Expected element " + element + " should be visible : but found not visible");
			throw new AssertionError("Expected element " + element + " should be visible : but found not visible");
			//throw new Exception("Expected element " + element + " should be visible : but found not visible",localException);
		}
	}

	/**
	 * @param element
	 * @param val
	 */
	public void assertTextMatches(WebElement element, String val) throws Exception{
		try {
			waitForTextMatches(element, val);
		} catch (Exception localException) {
		String actualVal = element.getText();
		String msg = String.format("Expected %s text should be [%s] : but found [%s] ", new Object[] { element, val, actualVal });
		log.error("Expected text is: "+"["+val+"]"+" Actual text is: "+"["+actualVal+"]");
		throw new AssertionError(msg);
		//throw new Exception("Expected text is: "+val+" Actual text is: "+actualVal,localException);
		
		}
	}

	/**
	 * @param element
	 * @param matcher
	 */
	public void waitForTextMatches(final WebElement element, final String matcher) {
		Wait<WebElement> wait = new FluentWait<WebElement>(element)

		.withTimeout(30, TimeUnit.SECONDS)

		.pollingEvery(5, TimeUnit.SECONDS)

		.ignoring(NoSuchElementException.class);

		wait.until(new Function<WebElement, Boolean>() {

			public Boolean apply(WebElement element) {

				try {
					String elementText = element.getText();
					return Boolean.valueOf(matcher.matches(elementText));
				} catch (StaleElementReferenceException e) {
				}
				return null;
			}

			public String toString() {
				return String.format("element ('%s') text %s",
						new Object[] { element, WebDriverCommonLib.this.toString() });
			}
		});
	}

	public void waitForAjaxToComplete(long... second) {
		try {
			(new WebDriverWait(driver, 60)).until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver d) {
					JavascriptExecutor js = (JavascriptExecutor) d;
					return (Boolean) js.executeScript("return !!window.jQuery && window.jQuery.active == 0");

				}
			});
		} catch (Exception e) {
		}
	}

	public void waitForLoaderToDismiss() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		try {
			wait.until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver wdriver) {

					return ((JavascriptExecutor) wdriver).executeScript("return document.readyState")
							.equals("complete");
				}
			});
		} catch (Exception e) {
			System.out.println("Waiting for Loader to dismiss timed out");
		}
	}

	public void refreshPage() {
		driver.navigate().refresh();
	}

	public void clickBrowserBackButton() {
		waitForLoaderToDismiss();
		driver.navigate().back();
		pause(2000);
	}

	/**
	 * @param element
	 * @param timeOut
	 * @return
	 */
	public WebElement waitForElementToBeClickable(WebElement element) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 15);
			element = wait.until(ExpectedConditions.elementToBeClickable(element));
		} catch (Exception e) {
		}
		return element;
	}

	public long getRandomNumber() {
		return Math.round(Math.random() * 10000);
	}

	public boolean isSortedList(List<WebElement> lstItem) {
		boolean flag = false;
		List<String> studentNames = new ArrayList<String>();
		for (WebElement ele : lstItem) {
			studentNames.add(ele.getText());
		}
		List<String> sortedList = new ArrayList<String>(studentNames);
		Collections.sort(sortedList);
		flag = sortedList.equals(studentNames);
		return flag;
	}

	public boolean verifyElementIsClickable(WebElement webe) {

		try {
			WebDriverWait wait = new WebDriverWait(driver, 5);
			wait.until(ExpectedConditions.elementToBeClickable(webe));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// Method to switch to old window and close current one
	/**
	 * @param oldWindow
	 * @return String oldWindow = driver.getWindowHandle();
	 *         cLib.closeAndSwitchToOldWindow(oldWindow);
	 */
	public boolean closeAndSwitchToOldWindow(String oldWindow) {

		if (driver.getWindowHandles().size() > 1) {
			for (String window : driver.getWindowHandles()) {
				if (!window.equals(oldWindow)) {
					driver.switchTo().window(window);
					driver.close();
				}
			}
			driver.switchTo().window(oldWindow);
			pause(1000);
			return true;
		}
		return false;
	}

	public String removeSpecialCharacterFromString(String str) {
		return str.replaceAll("[^\\x00-\\x7F]", "");
	}

	public boolean isAttribtuePresent(WebElement element, String attribute) {
		Boolean result = false;
		try {
			String value = element.getAttribute(attribute);
			if (value != null) {
				result = true;
			}
		} catch (Exception e) {
		}

		return result;
	}

	public void clickOnCenterOfElement(WebElement ele) {
		Actions action = new Actions(driver);
		Dimension d = ele.getSize();
		Point p = ele.getLocation();
		System.out.println("#############X:>" + ((p.getX() + d.getWidth()) / 2));
		System.out.println("#############X1:>" + p.getX());
		System.out.println("#############Y:>" + p.getY());
		System.out.println("#############Y1:>" + ((p.getY() + d.getHeight()) / 2));
		action.moveToElement(ele, (p.getX() + d.getWidth()) / 2, (p.getY() + d.getHeight()) / 2).click().build()
				.perform();
	}

}