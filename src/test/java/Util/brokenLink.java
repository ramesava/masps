/**
 * 
 */
package Util;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * @author RLE0293
 *
 */
public class brokenLink {

	public String isLinkBroken(URL url) throws Exception

	{

		String response = "";

		HttpURLConnection connection = (HttpURLConnection) url.openConnection();

		try

		{

			connection.connect();

			response = connection.getResponseMessage();

			connection.disconnect();

			return response;

		}

		catch (Exception exp)

		{

			return exp.getMessage();

		}

	}
}
