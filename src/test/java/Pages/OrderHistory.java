package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class OrderHistory {

	@FindBy(id = "orderHistoryTab")
	private WebElement order;

	@FindBy(xpath = "//*[@id ='orderDetailHdr-Left'] | //*[@id='printData']/../preceding-sibling::*")
	private WebElement orderDate;

	@FindBy(xpath = "//*[@class ='order-details-wrapper']")
	private WebElement orderWeb;

	@FindBy(xpath = "//*[text()='Return to Order History']")
	private WebElement returnOrder;

	@FindBy(xpath = "(//*[starts-with(@id,'ofe')])[1]/descendant::*[@class='view-order-details']/a")
	private WebElement orderD;

	@FindBy(xpath = "(//*[starts-with(@id,'ofe')])[1]/descendant::*[@class='oh_label_right']/a")
	private WebElement orderNo;

	public void orderLnk() {
		order.click();
	}

	public void viewOrderLnk() {
		orderD.click();
	}

	public WebElement orderDate() {
		return orderDate;
	}

	public void returnOrderHLnk() {
		returnOrder.click();
	}

	public WebElement orderWeb() {
		return orderD;

	}

	public WebElement orderWebE() {
		return orderWeb;

	}

	public void orderNoLnk() {
		orderNo.click();
	}

}
