package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class pwdChange {

	@FindBy(id = "editChangePassword")
	private WebElement changePwdLink;

	@FindBy(id = "current2")
	private WebElement cPwd;

	@FindBy(id = "password2")
	private WebElement nPwd;

	@FindBy(id = "newPassword")
	private WebElement ncPwd;

	@FindBy(id = "continueEditPassword2")
	private WebElement contBtn;

	@FindBy(id = "frame_close2")
	private WebElement frmclose;

	@FindBy(id = "loginId2")
	private WebElement loginId;

	@FindBy(id = "password2")
	private WebElement pwd;

	@FindBy(id = "nextbutton")
	private WebElement nxtBtn;

	@FindBy(id = "cancelEditPassword2")
	private WebElement cancelBtn;

	public void cancelBtn() {
		cancelBtn.click();
	}

	public void pChangeLnk() {
		changePwdLink.click();
	}

	public String cPwd(String input) {
		cPwd.clear();
		cPwd.sendKeys(input);
		return input;
	}

	public String nPwd(String input) {
		nPwd.clear();
		nPwd.sendKeys(input);
		return input;
	}

	public String ncPwd(String input) {
		ncPwd.clear();
		ncPwd.sendKeys(input);
		return input;
	}

	public void cBtn() {
		contBtn.click();
	}

	public String loginId(String input) {
		loginId.clear();
		loginId.sendKeys(input);
		return input;
	}

	public String LoginPwd(String input) {
		pwd.clear();
		pwd.sendKeys(input);
		return input;
	}

	public void nxtBtn() {
		nxtBtn.click();
	}

	public WebElement changePwd() {
		return changePwdLink;
	}

}