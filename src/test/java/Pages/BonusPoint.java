package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BonusPoint {

	@FindBy(id = "bonusPointTab")
	private WebElement bonus;

	@FindBy(xpath = "//*[text()='Balance:']/following-sibling::dd/strong")
	private WebElement bonusPt;

	@FindBy(xpath = "//*[text()='Earned this year:']/following-sibling::dd")
	private WebElement thisYrPt;

	public void bonusTab() {
		bonus.click();
	}

	public String bonusPt() {
		return bonusPt.getText();

	}

	public String thisYrPt() {
		return thisYrPt.getText();

	}

	public WebElement bonusWeb() {

		return bonusPt;
	}
}
