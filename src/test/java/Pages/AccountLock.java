package Pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AccountLock {

	@FindBy(id = "SIGN_IN_USER_INVALID")
	private WebElement invalid;

	@FindBy(id = "PCI_USER_ACCOUNT_LOCKED")
	private List<WebElement> lockedL;

	@FindBy(id = "PCI_USER_ACCOUNT_LOCKED")
	private WebElement locked;

	@FindBy(id = "PCI_USER_ACCOUNT_LOCKED")
	private WebElement lockedWeb;

	public String invalid() {
		return invalid.getText();
	}

	public String actLkd() {
		return locked.getText();
	}

	public List<WebElement> actLkdL() {
		return lockedL;
	}

	public WebElement actLkdWeb() {
		return lockedWeb;
	}

	public WebElement invalidWeb() {
		return invalid;
	}

}
