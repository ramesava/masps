package Pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class USSchoolList {

	@FindBy(id = "goRegistration")
	private WebElement singnUpLink;

	@FindBy(xpath = "//*[@class='errorLbl']")
	private WebElement singnInError;

	@FindBy(id = "firstNameMyProfile")
	private WebElement NameTxt;

	@FindBy(id = "emailMyProfile")
	private WebElement emailTxt;

	@FindBy(id = "schLocMBase")
	private WebElement mBaseSel;

	@FindBy(xpath = "//*[@id='teacherRegisterNow']/a")
	private WebElement eduRegLink;

	@FindBy(xpath = "//*[@id='mya_registerParent']/a")
	private WebElement parentRegLink;

	@FindBy(xpath = "//select[@id='salutations']")
	private WebElement Title;

	@FindBy(id = "firstName")
	private WebElement Fname;

	@FindBy(id = "lastName")
	private WebElement Lname;

	@FindBy(id = "email")
	private WebElement email;

	@FindBy(id = "cnfEmail")
	private WebElement cemail;

	@FindBy(id = "pwd")
	private WebElement pwd;

	@FindBy(id = "cnfpwd")
	private WebElement cpwd;

	@FindBy(id = "next")
	private WebElement nxt;

	@FindBy(xpath = "//select[@id ='stateWid']")
	private WebElement state;

	@FindBy(xpath = "//select[@id ='cityWid']")
	private WebElement city;

	@FindBy(xpath = "//select[@id ='schools']")
	private WebElement school;

	@FindBy(id = "terms")
	private WebElement termschk;

	@FindBy(id = "privacy")
	private WebElement privacychk;

	@FindBy(xpath = "(//*[@id='grade'])[1]")
	private WebElement gradechkbox;

	@FindBy(xpath = "//*[@id='10023-students']")
	private WebElement classSize;

	@FindBy(xpath = "//*[@id='sps_teacher_newuser']/h3")
	private WebElement cnfm;

	@FindBy(id = "frame_close2")
	private WebElement frmclose;

	@FindBy(id = "first_name")
	private WebElement cFName;

	@FindBy(id = "last_name")
	private WebElement cLName;

	@FindBy(xpath = "//select[@id ='grades']")
	private WebElement grade;

	@FindBy(id = "Girl")
	private WebElement gender;

	@FindBy(id = "dom")
	private WebElement dom;

	@FindBy(id = "doy")
	private WebElement doy;

	@FindBy(id = "svBtn")
	private WebElement svBtn;

	@FindBy(id = "parent_skipstep2-btm")
	private WebElement parentSkipBtn;

	@FindBy(xpath = "//*[@id='reg_window']//h3")
	private WebElement parentCnfmMsg;

	@FindBy(id = "loginId2")
	private WebElement loginID;

	@FindBy(id = "password2")
	private WebElement loginPwd;

	@FindBy(id = "nextbutton")
	private WebElement nxtBtn;

	@FindBy(id = "bcoe")
	private WebElement bcoeNo;

	@FindBy(id = "zipCodeTextBox")
	private WebElement zipCodeTxt;

	@FindBy(id = "findSchoolByZip")
	private WebElement findSchoolBtn;

	@FindBy(xpath = "//*[@id='errors']/a|//*[@id='errors']/font")
	private List<WebElement> error;

	@FindBy(id = "errors")
	private WebElement errorTxt;

	@FindBy(xpath = "//*[text()='Customer Number:']")
	private WebElement customerNoTxt;

	public void findSchoolBtn() {
		findSchoolBtn.click();

	}

	public void signUplnk() {
		singnUpLink.click();

	}

	public void militaryBase() {
		mBaseSel.click();

	}

	public void eduRegLnk() {
		eduRegLink.click();

	}

	public void parentRegLnk() {
		parentRegLink.click();

	}

	public String selectTitle(String input) {
		Select sel1 = new Select(Title);
		sel1.selectByValue(input);
		return input;

	}

	public String Fname(String input) {
		Fname.sendKeys(input);
		return input;
	}

	public String zipCode(String input) {
		zipCodeTxt.sendKeys(input);
		return input;
	}

	public String Lname(String input) {
		Lname.sendKeys(input);
		return input;
	}

	public String email(String input) {
		email.sendKeys(input);
		return input;
	}

	public String cemail(String input) {
		cemail.sendKeys(input);
		return input;
	}

	public String pwd(String input) {
		pwd.sendKeys(input);
		return input;
	}

	public String cpwd(String input) {
		cpwd.sendKeys(input);
		return input;
	}

	public void clknxt() {
		nxt.click();
	}

	public String SelectState(String input) {
		Select sel1 = new Select(state);
		sel1.selectByValue(input);
		return input;

	}

	public String SelectCity(String input) {
		Select sel1 = new Select(city);
		sel1.selectByValue(input);
		return input;

	}

	public void SelectSchool() {
		Select sel1 = new Select(school);
		sel1.selectByIndex(2);

	}

	public void terms() {
		termschk.click();
	}

	public void privacy() {
		privacychk.click();
	}

	public void gradechkBx() {
		gradechkbox.click();
	}

	public String classSize(String input) {
		classSize.sendKeys(input);
		return input;
	}

	public String cnfmMsg() {
		return cnfm.getText();

	}

	public void frmClz() {
		frmclose.click();
	}

	public String cFname(String input) {
		cFName.sendKeys(input);
		return input;
	}

	public String cLname(String input) {
		cLName.sendKeys(input);
		return input;
	}

	public void SelectGrade() {
		Select gSel = new Select(grade);
		gSel.selectByIndex(2);

	}

	public void gender() {
		gender.click();
	}

	public void SelectMonth() {
		Select mSel = new Select(dom);
		mSel.selectByIndex(2);

	}

	public void SelectYear() {
		Select ySel = new Select(doy);
		ySel.selectByIndex(2);

	}

	public void svBtn() {
		svBtn.click();
	}

	public WebElement dspSvBtn() {
		return svBtn;
	}

	public void parentSkipBtn() {
		parentSkipBtn.click();
	}

	public WebElement skipBtn() {
		return parentSkipBtn;
	}

	public String parentCnfmMsg() {
		return parentCnfmMsg.getText();

	}

	public WebElement parentCnfmMsgWeb() {
		return parentCnfmMsg;

	}

	public String signInError() {
		return singnInError.getText();

	}

	public String nUsrEmail(String input) {
		loginID.sendKeys(input);
		return input;
	}

	public String nUsrPwd(String input) {
		loginPwd.sendKeys(input);
		return input;
	}

	public void nxtBtn() {
		nxtBtn.click();
	}

	public String bcoe() {
		return bcoeNo.getText();
	}

	public WebElement customerNoTxt() {
		return customerNoTxt;
	}

	public WebElement nameWel() {
		return NameTxt;
	}

	public String nameText() {
		return NameTxt.getText();
	}

	public String emailText() {
		return emailTxt.getText();
	}

	public List<WebElement> errorSize() {

		return error;
	}

	public String errorTxt() {
		return errorTxt.getText();
	}

	public WebElement errorWeb() {
		return errorTxt;
	}

	public WebElement cnfWeb() {
		return cnfm;
	}

	public WebElement emailWeb() {
		return emailTxt;
	}
	
	public WebElement signInWeb() {
		return singnInError;
	}
	
	
}
