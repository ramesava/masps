package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class profileQEdit {

	@FindBy(id = "editChangeQuestion")
	private WebElement pQELink;

	@FindBy(id = "cancelEditQuestion")
	private WebElement cancelBtn;

	@FindBy(id = "READING_LEVEL_KEY")
	private WebElement readingLSel;

	@FindBy(id = "newTeacherNo")
	private WebElement newTeacher;

	@FindBy(id = "mentorYes")
	private WebElement newMentor;

	@FindBy(id = "usedBookYes")
	private WebElement bookBtn;

	@FindBy(id = "birthMonth")
	private WebElement bMonth;

	@FindBy(id = "birthDay")
	private WebElement bDay;

	@FindBy(id = "continueEditQuestion")
	private WebElement continueBtn;

	@FindBy(id = "profile_questions")
	private WebElement profileQ;

	public void profileQELnk() {
		pQELink.click();
	}

	public void cancelBtn() {
		cancelBtn.click();
	}

	public void readingLvlSel() {
		Select sel = new Select(readingLSel);
		sel.selectByIndex(2);

	}

	public void teacherBtn() {
		newTeacher.click();
	}

	public void mentorBtn() {
		newMentor.click();
	}

	public void bookBtn() {
		bookBtn.click();
	}

	public void BMonthSel() {
		Select sel = new Select(bMonth);
		sel.selectByIndex(2);

	}

	public void BDaySel() {
		Select sel = new Select(bDay);
		sel.selectByIndex(6);

	}

	public void contineBtn() {
		continueBtn.click();
	}

	public WebElement profileQ() {
		return profileQ;
	}

}
