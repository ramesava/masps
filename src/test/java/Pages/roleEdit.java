package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class roleEdit {

	@FindBy(id = "teacher_profile_editroles-link")
	private WebElement editRoleLink;

	@FindBy(id = "reset")
	private WebElement reset;

	@FindBy(id = "cancelEditRoles")
	private WebElement cancelBtn;

	@FindBy(xpath = "//*[@value='10029']")
	private WebElement grade;

	@FindBy(id = "10029-students")
	private WebElement size;

	@FindBy(id = "continueEditRoles")
	private WebElement contineBtn;

	@FindBy(id = "rolesMyProfile")
	private WebElement roles;

	public void roleEdtLnk() {
		editRoleLink.click();
	}

	public void cancelBtn() {
		cancelBtn.click();
	}

	public String classSize(String input) {
		size.sendKeys(input);
		return input;
	}

	public String rolesTxt() {
		return roles.getText();

	}

	public void reset() {
		reset.click();
	}

	public void continueBtn() {
		contineBtn.click();
	}

	public void grade() {
		grade.click();
	}

	public WebElement roleTxt() {
		return roles;
	}

	public WebElement gradeTxt() {
		return grade;

	}

}