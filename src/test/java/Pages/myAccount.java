package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class myAccount {

	@FindBy(id = "myProfileTab")
	private WebElement myProfileLink;

	@FindBy(id = "Addresses")
	private WebElement addressLink;

	@FindBy(id = "orderHistoryTab")
	private WebElement orderHistoryLink;

	@FindBy(id = "billingPaymentTab")
	private WebElement billingPaymentLink;

	@FindBy(id = "subscriptionTab")
	private WebElement subscriptionLink;

	@FindBy(id = "bonusPointTab")
	private WebElement bonusPtLink;

	@FindBy(id = "destinationsTab")
	private WebElement MyDestLink;

	@FindBy(id = "downloadTab")
	private WebElement myDownloadLink;

	@FindBy(xpath = "//*[@id='profile-left-float']/h2")
	private WebElement profilePlaceholder;

	@FindBy(xpath = "//*[@id='address-box']/h2")
	private WebElement addressPlaceholder;

	@FindBy(xpath = "//*[@id='orderHistory']")
	private WebElement orderPlaceholder;

	@FindBy(xpath = "//*[@id='billing_homeleft-hdr']/h2")
	private WebElement billingPlaceholder;

	@FindBy(xpath = "//*[@id='subs_main-top']/h2")
	private WebElement subscriptionPlaceholder;

	@FindBy(xpath = "//*[@id='destinationsTab']")
	private WebElement myDestPlaceholder;

	@FindBy(xpath = "//*[@id='downloadTab']")
	private WebElement downloadPlaceholder;

	@FindBy(id = "frame_close2")
	private WebElement frmclose;

	/**
	 * 
	 * @param method
	 * @param input
	 * @return
	 * @throws InterruptedException
	 */

	public String getByName(String method, String input) throws InterruptedException {
		String output = "";
		if (method.equals("My Profile")) {
			output = myProfile(input);
		} else if (method.equals("Addresses")) {
			output = address(input);
		} else if (method.equals("Order History")) {
			output = orderHistory(input);
		} else if (method.equals("Billing & Payments")) {
			output = billingPayment(input);
		} else if (method.equals("Subscriptions")) {
			output = subscription(input);
		} else if (method.equals("My Destinations")) {
			output = MyDest(input);
		}

		else if (method.equals("My Downloads")) {
			output = myDownload(input);
		}
		return output;
	}

	public String myProfile(String input) {
		if (input.equalsIgnoreCase("attribute")) {
			String value = myProfileLink.getAttribute("href");
			return value;
		} else if (input.equalsIgnoreCase("click")) {
			String value = profilePlaceholder.getText();
			return value;
		} else if (input.equalsIgnoreCase("attribute_text")) {
			String value = myProfileLink.getText();
			return value;
		}
		return input;

	}

	public String address(String input) {
		if (input.equalsIgnoreCase("attribute")) {
			String value = addressLink.getAttribute("href");
			return value;
		} else if (input.equalsIgnoreCase("click")) {
			addressLink.click();
			String value = addressPlaceholder.getText();
			return value;
		} else if (input.equalsIgnoreCase("attribute_text")) {
			String value = addressLink.getText();
			return value;
		}
		return input;

	}

	public String orderHistory(String input) {
		if (input.equalsIgnoreCase("attribute")) {
			String value = orderHistoryLink.getAttribute("href");
			return value;
		} else if (input.equalsIgnoreCase("click")) {
			orderHistoryLink.click();
			String value = orderPlaceholder.getText();
			return value;
		} else if (input.equalsIgnoreCase("attribute_text")) {
			String value = orderHistoryLink.getText();
			return value;
		}
		return input;

	}

	public String billingPayment(String input) {
		if (input.equalsIgnoreCase("attribute")) {
			String value = billingPaymentLink.getAttribute("href");
			return value;
		} else if (input.equalsIgnoreCase("click")) {
			billingPaymentLink.click();
			String value = billingPlaceholder.getText();
			return value;
		} else if (input.equalsIgnoreCase("attribute_text")) {
			String value = billingPaymentLink.getText();
			return value;
		}
		return input;

	}

	public String subscription(String input) {
		if (input.equalsIgnoreCase("attribute")) {
			String value = subscriptionLink.getAttribute("href");
			return value;
		} else if (input.equalsIgnoreCase("click")) {
			subscriptionLink.click();
			String value = subscriptionPlaceholder.getText();
			return value;
		} else if (input.equalsIgnoreCase("attribute_text")) {
			String value = subscriptionLink.getText();
			return value;
		}
		return input;

	}

	public String MyDest(String input) throws InterruptedException {
		if (input.equalsIgnoreCase("attribute")) {
			String value = MyDestLink.getAttribute("href");
			return value;
		} else if (input.equalsIgnoreCase("click")) {
			MyDestLink.click();
			Thread.sleep(3000);
			String value = myDestPlaceholder.getText();
			return value;
		} else if (input.equalsIgnoreCase("attribute_text")) {
			String value = MyDestLink.getText();
			return value;
		}
		return input;

	}

	public String myDownload(String input) throws InterruptedException {
		if (input.equalsIgnoreCase("attribute")) {
			String value = myDownloadLink.getAttribute("href");
			return value;
		} else if (input.equalsIgnoreCase("click")) {
			myDownloadLink.click();
			Thread.sleep(3000);
			String value = downloadPlaceholder.getText();
			return value;
		} else if (input.equalsIgnoreCase("attribute_text")) {
			String value = myDownloadLink.getText();
			return value;
		}
		return input;

	}

	public void frmClz() {
		frmclose.click();
	}

}
