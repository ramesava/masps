package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class editCard {

	@FindBy(id = "exp_month")
	private WebElement eMonth;

	@FindBy(id = "exp_year")
	private WebElement eYear;

	@FindBy(id = "bill_to_forename_temp")
	private WebElement fName;

	@FindBy(id = "bill_to_surname_temp")
	private WebElement lName;

	@FindBy(id = "bill_to_address_line1_temp")
	private WebElement adrLine1;

	@FindBy(id = "bill_to_address_line2_temp")
	private WebElement adrLine2;

	@FindBy(id = "newEmail")
	private WebElement email;

	@FindBy(id = "saveButton")
	private WebElement svBtn;

	@FindBy(id = "billingPaymentTab")
	private WebElement billTab;

	@FindBy(xpath = "(//a[contains(@class,'editcard')])[1]")
	private WebElement editCard;

	@FindBy(xpath = "//*[@id='bill_addcard']")
	private WebElement addCrd;

	public String eMonth(String input) {
		Select sel2 = new Select(eMonth);
		sel2.selectByValue(input);
		return input;
	}

	public String eYear(String input) {
		Select sel3 = new Select(eYear);
		sel3.selectByValue(input);
		return input;
	}

	public String fName(String input) {
		fName.clear();
		fName.sendKeys(input);
		return input;
	}

	public String lName(String input) {
		lName.clear();
		lName.sendKeys(input);
		return input;
	}

	public String adrLine1(String input) {
		adrLine1.clear();
		adrLine1.sendKeys(input);
		return input;
	}

	public String adrLine2(String input) {
		adrLine2.clear();
		adrLine2.sendKeys(input);
		return input;
	}

	public String nEmail(String input) {
		email.clear();
		email.sendKeys(input);
		return input;
	}

	public void billTab() {
		billTab.click();

	}

	public void edtcard() {
		editCard.click();

	}

	public WebElement addCard() {
		return addCrd;
	}

	public void svBtn() {
		svBtn.click();

	}

}
