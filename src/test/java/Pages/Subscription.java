package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Subscription {

	@FindBy(id = "subscriptionTab")
	private WebElement subTab;

	@FindBy(xpath = "//*[@alt='Edit Billing Info']")
	private WebElement BilingInfo;

	@FindBy(xpath = "(//*[@id='creditCard']//input)[1]")
	private WebElement creditCrd;

	@FindBy(id = "newEmail")
	private WebElement newEmail;

	@FindBy(id = "saveButton")
	private WebElement saveBtn;

	@FindBy(xpath = "//*[@id='subs_main-top']/h2")
	private WebElement subText;

	@FindBy(xpath = "//*[@class='first']/a")
	private WebElement subPage;

	@FindBy(xpath = "(//h3)[1]")
	private WebElement subStatus;

	public void subTab() {
		subTab.click();
	}

	public void bilingInfoEdt() {
		BilingInfo.click();
	}

	public void creditCrdRadio() {
		creditCrd.click();
	}

	public String email(String input) {
		newEmail.sendKeys(input);
		return input;
	}

	public void saveBtn() {
		saveBtn.click();
	}

	public WebElement SubText() {
		return subText;
	}

	public void subPage() {
		subPage.click();
	}

	public String subStatusTxt() {
		return subStatus.getText();
	}

	public WebElement subStatus() {
		return subStatus;
	}

}
