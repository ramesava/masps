package Pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class destination {

	@FindBy(id = "destinationsTab")
	private WebElement desTab;

	@FindBy(xpath = "//*[contains(@class,'sps_teacher_poplinks')]//a")
	private List<WebElement> links;

	public List<WebElement> destLnk() {
		return links;
	}

	public void desLnk() {
		desTab.click();
	}

}
