package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;


public class pwdReset{

	@FindBy(id = "forgotAccountLnk")
	private WebElement forgotacctLnk;
	
/*	@FindBy(locator = "ma2.frg.lnk")
	private WebElement forgotacctLnk1;*/
	
	
	@FindBy(id = "email_area")
	private WebElement emailArea;
	
	
	@FindBy(id = "login_area-pword")
	private WebElement logInArea;
	

	@FindBy(id = "fNameID")
	private WebElement fName;

	@FindBy(id = "lNameID")
	private WebElement lName;

	@FindBy(id = "stateWid")
	private WebElement state;

	@FindBy(id = "cityWid")
	private WebElement city;
	
	@FindBy(xpath = "//*[@value='New York']")
	private WebElement cityWeb;
	
	@FindBy(xpath = "//*[@value='23006422']")
	private WebElement schoolWeb;
	
	

	@FindBy(id = "forgotAccountSubmit")
	private WebElement forgotBtn;

	@FindBy(xpath = "//*[@value='getpass']")
	private WebElement emailBtn;
	
	@FindBy(xpath = "//*[@value='login']")
	private WebElement loginBtn;

	@FindBy(id = "forgotAccountCancel")
	private WebElement forgotcancelLnk;

	@FindBy(id = "forgotEmailCancel")
	private WebElement forgotEmailcancelLnk;

	@FindBy(id = "emailRecover")
	private WebElement emailTxt;

	@FindBy(id = "forgotAccountSubmit")
	private WebElement submitBtn;

	@FindBy(xpath = "//*[@class='pr_account']")
	private WebElement cnfMsg;

	@FindBy(xpath = "//*[contains(text(),'Retrieve email')]")
	private WebElement retrieveEmail;

	@FindBy(id = "zipCodeTextBox")
	private WebElement zipcode;

	@FindBy(id = "findSchoolByZip")
	private WebElement schoolZipcode;

	@FindBy(xpath = "//*[@id='email_area-content']/h3")
	private WebElement EmailTxt;

	@FindBy(xpath = "(//*[@id='emailShow']/input[@name='emailRecover'])[1]")
	private WebElement EmailRecRadio;

	@FindBy(id = "forgotEmailRedirect")
	private WebElement emailRedirectBtn;

	@FindBy(id = "frame_close2")
	private WebElement frmclose;

	@FindBy(id = "schools")
	private WebElement school;
	
	@FindBy(id = "loginId2")
	private WebElement loginID;
	
	@FindBy(id = "emailRedirectForm")
	private WebElement emailRedForm;
	
	
	@FindBy(xpath = "//*[@id='forgotAccountCancel2'] | //*[@id='forgotEmailReturn']")
	private WebElement emailReturnBtn;
	

	public void forgotAcctLnk() {
		forgotacctLnk.click();

	}

	public void emailRedirectBtn() {
		emailRedirectBtn.click();

	}

	public void EmailRecRadio() {
		EmailRecRadio.click();

	}

	public void emailBtn() {
		emailBtn.click();

	}
	
	public void loginBtn() {
		loginBtn.click();

	}

	
	public void forgotBtn() {
		forgotBtn.click();

	}

	public void submitBtn() {
		submitBtn.click();

	}

	public void forgotCancelLnk() {
		forgotcancelLnk.click();

	}

	public void retrieveEmailLnk() {
		retrieveEmail.click();

	}

	public String emailRec(String input) {
		emailTxt.sendKeys(input);
		return input;

	}

	public String fName(String input) {
		fName.sendKeys(input);
		return input;

	}

	public String state(String input) {
		Select sel1 = new Select(state);
		sel1.selectByValue(input);
		return input;

	}

	public String city(String input) {
		Select sel1 = new Select(city);
		sel1.selectByValue(input);
		return input;

	}

	public void schoolSelect(String input) {
		Select sel1 = new Select(school);
		sel1.selectByValue(input);

	}

	public String lName(String input) {
		lName.sendKeys(input);
		return input;

	}

	public String byZipCode(String input) {
		zipcode.sendKeys(input);
		return input;

	}

	public void schoolByZip() {
		schoolZipcode.click();

	}

	public String EmailTxt() {
		return EmailTxt.getText();

	}

	public String cnfMsg() {
		return cnfMsg.getText();

	}

	public void forgotEmaillLnk() {
		forgotEmailcancelLnk.click();

	}

	public void frmClz() {
		frmclose.click();
	}

	public WebElement school() {
		return school;

	}

	public WebElement cnfMesg() {
		return cnfMsg;

	}

	public WebElement forgotActLnk() {
		return forgotacctLnk;

	}
	
	public WebElement loginID() {
		return loginID;

	}
	
	public WebElement EmailrecWeb() {
		return EmailRecRadio;

	}
	
	public WebElement emailAreaWeb() {
		return emailArea;

	}
	
	public WebElement logInAreaWeb() {
		return logInArea;

	}
	
	
	public WebElement emailWeb() {
		return emailTxt;

	}	
	
	public WebElement emailRedWeb() {
		return emailRedForm;

	}
	
	public void emailRetWeb() {
		emailReturnBtn.click();
	}	
	
	public WebElement emailReWeb() {
		return emailReturnBtn;

	}
	
	public WebElement cityWeb() {
		return cityWeb;

	}
	
	public WebElement schoolWeb() {
		return schoolWeb;

	}
	

}
