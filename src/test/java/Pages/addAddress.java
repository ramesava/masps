package Pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class addAddress {

	@FindBy(id = "description")
	private WebElement des;

	@FindBy(id = "firstName")
	private WebElement fName;

	@FindBy(id = "lastName")
	private WebElement lName;

	@FindBy(id = "AddressLine1")
	private WebElement addLine1;

	@FindBy(id = "AddressLine2")
	private WebElement addLine2;

	@FindBy(id = "postal_code")
	private WebElement postalCode;

	@FindBy(id = "address_citystate_lookup")
	private WebElement cityState;

	@FindBy(id = "saveButton")
	private WebElement svBtn;

	@FindBy(id = "Addresses")
	private WebElement addrTab;

	@FindBy(id = "newaddressget")
	private WebElement addAddr;

	@FindBy(id = "frame_close2")
	private WebElement frmclose;

	@FindBy(xpath = "//*[@id='single_org_0']/p/a")
	private WebElement editLnk;

	@FindBy(id = "schChngDifAddress")
	private WebElement changeAdd;

	@FindBy(id = "schoolName")
	private WebElement school;

	@FindBy(id = "continueButton")
	private WebElement continueBtn;

	@FindBy(id = "address-box")
	private WebElement AddressOrg;

	@FindBy(id = "address-box")
	private List<WebElement> AddressOrgL;

	@FindBy(xpath = "//*[@id='single_address_0']/p/a[contains(@class,'address_edit-link')]")
	private WebElement editAdrLnk;

	@FindBy(id = "cancelButton")
	private WebElement cancelBtn;

	@FindBy(id = "errorEdit")
	private WebElement error;

	@FindBy(xpath = "//*[@id='single_address_0']/p//a[2]")
	private WebElement deleteLnk;

	public String description(String input) {
		des.clear();
		des.sendKeys(input);
		return input;
	}

	public String fName(String input) {
		fName.clear();
		fName.sendKeys(input);
		return input;
	}

	public String lName(String input) {
		lName.clear();
		lName.sendKeys(input);
		return input;
	}

	public String adrLine1(String input) {
		addLine1.clear();
		addLine1.sendKeys(input);
		return input;
	}

	public String adrLine2(String input) {
		addLine2.clear();
		addLine2.sendKeys(input);
		return input;
	}

	public String postalCode(String input) {
		postalCode.clear();
		postalCode.sendKeys(input);
		return input;
	}

	public void ctyState() {
		cityState.click();
	}

	public void svBtn() {
		svBtn.click();

	}

	public void frmClz() {
		frmclose.click();
	}

	public void editLink() {
		editLnk.click();

	}

	public String schoolName(String input) {
		school.sendKeys(input);
		return input;
	}

	public String error() {

		return error.getText();
	}

	public void changeAdd() {
		changeAdd.click();

	}

	public void continueBtn() {
		continueBtn.click();
	}

	public void addrTab() {
		addrTab.click();
	}

	public void addAddr() {
		addAddr.click();
	}

	public void editAddrLnk() {
		editAdrLnk.click();
	}

	public void cancelBtn() {
		cancelBtn.click();
	}

	public void deleteBtn() {
		deleteLnk.click();
	}

	public WebElement addressOrg() {
		return AddressOrg;
	}

	public List<WebElement> addressOrgL() {
		return AddressOrgL;
	}

}
