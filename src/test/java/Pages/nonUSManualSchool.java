package Pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class nonUSManualSchool {

	@FindBy(id = "schLocNonUSSch")
	private WebElement schoolLoc;

	@FindBy(id = "toManualList")
	private WebElement manualSchoolLnk;

	@FindBy(id = "schoolCountry")
	private WebElement country;

	@FindBy(id = "country")
	private WebElement cntry;

	@FindBy(id = "schoolName")
	private WebElement schoolName;

	@FindBy(id = "type")
	private WebElement schoolType;

	@FindBy(id = "line1")
	private WebElement addLine1;

	@FindBy(id = "city")
	private WebElement city;

	@FindBy(id = "state")
	private WebElement state;

	@FindBy(id = "zipCode")
	private WebElement zip;

	@FindBy(id = "phone")
	private WebElement phone;

	@FindBy(id = "terms")
	private WebElement termschk;

	@FindBy(id = "privacy")
	private WebElement privacychk;

	@FindBy(xpath = "(//*[@id='grade'])[1]")
	private WebElement gradechkbox;

	@FindBy(xpath = "//*[@id='10023-students']")
	private WebElement classSize;

	@FindBy(xpath = "//*[@id='sps_teacher_newuser']/h3")
	private WebElement cnfm;

	@FindBy(id = "frame_close2")
	private WebElement frmclose;

	@FindBy(id = "next")
	private WebElement nxt;

	@FindBy(xpath = "//*[@id='errors']/a|//*[@id='errors']/font")
	private List<WebElement> error;

	@FindBy(id = "errors")
	private WebElement errorTxt;

	public void schoolLoc() {
		schoolLoc.click();

	}

	public String country(String input) {
		Select sel1 = new Select(country);
		sel1.selectByValue(input);
		return input;

	}

	public WebElement contryDsp() {
		return country;
	}

	public void manualSLnk() {
		manualSchoolLnk.click();

	}

	public String Sname(String input) {
		schoolName.sendKeys(input);
		return input;
	}

	public String schoolType(String input) {
		Select sel1 = new Select(schoolType);
		sel1.selectByValue(input);
		return input;
	}

	public String AddLine1(String input) {
		addLine1.sendKeys(input);
		return input;
	}

	public String city(String input) {
		city.sendKeys(input);
		return input;
	}

	public String state(String input) {
		state.sendKeys(input);
		return input;
	}

	public String zipCode(String input) {
		zip.sendKeys(input);
		return input;
	}

	public String SelectCntry(String input) {
		Select sel1 = new Select(cntry);
		sel1.selectByValue(input);
		return input;

	}

	public String phone(String input) {
		phone.sendKeys(input);
		return input;
	}

	public void terms() {
		termschk.click();
	}

	public void privacy() {
		privacychk.click();
	}

	public void clknxt() {
		nxt.click();
	}

	public void gradechkBx() {
		gradechkbox.click();
	}

	public String classSize(String input) {
		classSize.sendKeys(input);
		return input;
	}

	public String cnfmMsg(String input) {
		cnfm.getText().contains(input);
		return input;
	}

	public void frmClz() {
		frmclose.click();
	}

	public List<WebElement> errorSize() {

		return error;
	}

	public String errorTxt() {
		return errorTxt.getText();
	}

	public WebElement errorWeb() {
		return errorTxt;
	}

}
