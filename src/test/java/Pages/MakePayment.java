package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MakePayment {

	@FindBy(id = "makePaymentOK")
	private WebElement makeP;

	@FindBy(id = "balance")
	private WebElement balance;

	@FindBy(id = "billing_payment-left-box")
	private WebElement billing;

	@FindBy(id = "partial")
	private WebElement partial;

	@FindBy(id = "payment_partial")
	private WebElement partialTxt;

	@FindBy(id = "billing_confirm_complete")
	private WebElement bilingCnfComp;

	@FindBy(xpath = "//*[@id='billing_confirm-box']/h2")
	private WebElement bilingCnf;

	@FindBy(id = "goBilling")
	private WebElement bilingBk;

	@FindBy(id = "payment_addcard_link")
	private WebElement addCrd;

	@FindBy(id = "bill_addcard")
	private WebElement addCrd1;

	public void mPaymentBtn() {
		makeP.click();

	}

	public WebElement addCrdLnk1() {
		return addCrd1;

	}

	public String balance() {
		return balance.getText();

	}

	public WebElement bilingWeb() {
		return billing;

	}

	public void partialBtn() {
		partial.click();

	}

	public void bilingCnfCmpBtn() {
		bilingCnfComp.click();

	}

	public String partilal(String input) {
		partialTxt.sendKeys(input);
		return input;
	}

	public String bilingCnfMsg() {
		return bilingCnf.getText();

	}

	public WebElement bilingCnfWeb() {
		return bilingCnf;

	}

	public String paymentCnfMsg() {
		return bilingCnf.getText();

	}

	public WebElement paymentCnfWeb() {
		return bilingCnf;

	}

	public void bilingBkBtn() {
		bilingBk.click();

	}

	public void addCrdLnk() {
		addCrd.click();

	}

}
