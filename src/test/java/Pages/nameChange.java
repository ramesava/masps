package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class nameChange {

	@FindBy(id = "editChangeName")
	private WebElement changeNameLink;

	@FindBy(id = "cancelEditName2")
	private WebElement cancelBtn;

	@FindBy(id = "change")
	private WebElement changeBtn;

	@FindBy(id = "continueEditName2")
	private WebElement contBtn;

	@FindBy(id = "title")
	private WebElement title;

	@FindBy(id = "Fname")
	private WebElement fName;

	@FindBy(id = "Lname")
	private WebElement lName;

	@FindBy(id = "editNameChange")
	private WebElement svBtn;

	@FindBy(id = "firstNameMyProfile")
	private WebElement fNameProfile;

	public void cancelBtn() {
		cancelBtn.click();
	}

	public void nChange() {
		changeNameLink.click();
	}

	public void changeBtn() {
		changeBtn.click();
	}

	public void cBtn() {
		contBtn.click();
	}

	public String titleDropDown(String input) {
		Select sel1 = new Select(title);
		sel1.selectByValue(input);
		return input;
	}

	public String firstName(String input) {
		fName.clear();
		fName.sendKeys(input);
		return input;
	}

	public String lastName(String input) {
		lName.clear();
		lName.sendKeys(input);
		return input;
	}

	public void saveBtn() {
		svBtn.click();
	}

	public WebElement fnameProfile() {
		return fNameProfile;
	}

}