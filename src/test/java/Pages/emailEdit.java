package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class emailEdit {

	@FindBy(id = "editChangeMail")
	private WebElement changeEmailLink;

	@FindBy(id = "newEmail")
	private WebElement nEmail;

	@FindBy(id = "cnfEmail")
	private WebElement cEmail;

	@FindBy(id = "continueEditMail2")
	private WebElement continueBtn;

	@FindBy(id = "emailMyProfile")
	private WebElement emailProfile;

	@FindBy(id = "cancelEditMail2")
	private WebElement cancelBtn;

	public void emailChangeLnk() {
		changeEmailLink.click();
	}

	public void cancelBtn() {
		cancelBtn.click();
	}

	public String newEmail(String input) {
		nEmail.clear();
		nEmail.sendKeys(input);
		return input;
	}

	public String cnfEmail(String input) {
		cEmail.clear();
		cEmail.sendKeys(input);
		return input;
	}

	public void cntBtn() {
		continueBtn.click();
	}

	public WebElement emailProfile() {
		return emailProfile;
	}

}