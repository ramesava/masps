package Pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class addCard {

	@FindBy(id = "card_type_dropdown")
	private WebElement cType;

	@FindBy(id = "card_number_temp")
	private WebElement cardNumber;

	@FindBy(id = "exp_month")
	private WebElement eMonth;

	@FindBy(id = "newEmail")
	private WebElement email;

	@FindBy(id = "exp_year")
	private WebElement eYear;

	@FindBy(id = "card_cvn_temp")
	private WebElement cardCVN;

	@FindBy(id = "bill_to_forename_temp")
	private WebElement fName;

	@FindBy(id = "bill_to_surname_temp")
	private WebElement lName;

	@FindBy(id = "bill_to_address_line1_temp")
	private WebElement adrLine1;

	@FindBy(id = "bill_to_address_line2_temp")
	private WebElement adrLine2;

	@FindBy(id = "bill_to_address_postal_code_temp")
	private WebElement postalCode;

	@FindBy(id = "billing_citystate_lookup")
	private WebElement cityState;

	@FindBy(id = "saveButton")
	private WebElement svBtn;

	@FindBy(id = "errors")
	private WebElement err;

	@FindBy(id = "billingPaymentTab")
	private WebElement pmtTab;

	@FindBy(id = "bill_addcard")
	private WebElement addCrd;

	@FindBy(id = "bill_addcard")
	private List<WebElement> addCrd1;

	@FindBy(id = "frame_close2")
	private WebElement frmclose;

	@FindBy(xpath = "//*[text()='State']")
	private WebElement stateDsp;

	@FindBy(xpath = "(//a[text()='Set as Default'])[1]")
	private WebElement setDefault;

	@FindBy(xpath = "//a/following-sibling::*[text()='Default Card']")
	private WebElement defaultCrd;

	@FindBy(xpath = "(//*[text()='Delete'])[1]")
	private WebElement alert;

	public String cardType(String input) {
		Select sel1 = new Select(cType);
		sel1.selectByValue(input);
		return input;
	}

	public String cardNumber(String input) {
		cardNumber.sendKeys(input);
		return input;
	}

	public String eMonth(String input) {
		Select sel2 = new Select(eMonth);
		sel2.selectByValue(input);
		return input;
	}

	public String eYear(String input) {
		Select sel3 = new Select(eYear);
		sel3.selectByValue(input);
		return input;
	}

	public String cCVN(String input) {
		cardCVN.sendKeys(input);
		return input;
	}

	public String fName(String input) {
		fName.sendKeys(input);
		return input;
	}

	public String lName(String input) {
		lName.sendKeys(input);
		return input;
	}

	public String adrLine1(String input) {
		adrLine1.sendKeys(input);
		return input;
	}

	public String adrLine2(String input) {
		adrLine2.sendKeys(input);
		return input;
	}

	public String postalCode(String input) {
		postalCode.sendKeys(input);
		return input;
	}

	public void ctyState() {
		cityState.click();
	}

	public void svBtn() {
		svBtn.click();

	}

	public String error() {
		String er = err.getText();
		return er;
	}

	public void pmtTab() {
		pmtTab.click();

	}

	public void setDefalut() {
		setDefault.click();

	}

	public void delete() {
		alert.click();

	}

	public void addCrd() {
		addCrd.click();

	}

	public void frmClz() {
		frmclose.click();
	}

	public WebElement stateDsp() {
		return stateDsp;
	}

	public WebElement addCrdLnk() {
		return addCrd;

	}

	public WebElement deafault() {
		return defaultCrd;

	}

	public List<WebElement> addCrdLnk1() {
		return addCrd1;

	}

	public String nEmail(String input) {
		email.clear();
		email.sendKeys(input);
		return input;
	}

}
