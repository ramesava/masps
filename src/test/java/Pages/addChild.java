package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class addChild {

	@FindBy(id = "alsoParentChk")
	private WebElement chkBox;

	@FindBy(id = "first_name")
	private WebElement fName;

	@FindBy(id = "last_name")
	private WebElement lName;

	@FindBy(id = "grades")
	private WebElement grd;

	@FindBy(id = "Girl")
	private WebElement gender;

	@FindBy(id = "dom")
	private WebElement month;

	@FindBy(id = "doy")
	private WebElement year;

	@FindBy(id = "cac_txt")
	private WebElement CAC;

	@FindBy(id = "SubmitActivationCodeButton")
	private WebElement SubmitBtn;

	@FindBy(id = "svBtn")
	private WebElement svBtn;
	
	
	@FindBy(id = "parent_chld_expand0")
	private WebElement parentExpand;
	
	
	@FindBy(xpath = "//*[@class='parent_childprofile-edit']")
	private WebElement childEdit;
	
	@FindBy(xpath = "//*[@class='parent_addchild-btmform-clear']")
	private WebElement childClear;
	
	@FindBy(xpath = "//*[@class='parent_childprofile-delete']")
	private WebElement deleteChild;
	
	
	public void addChildChkBox() {
		chkBox.click();
	}

	public String fName(String input) {
		fName.sendKeys(input);
		return input;
	}

	public String lName(String input) {
		lName.sendKeys(input);
		return input;
	}

	public String grade(String input) {
		Select sel1 = new Select(grd);
		sel1.selectByValue(input);
		return input;
	}

	public void gender() {
		gender.click();
	}

	public String monthDropDown(String input) {
		Select sel1 = new Select(month);
		sel1.selectByValue(input);
		return input;
	}

	public String yearDropDown(String input) {
		Select sel1 = new Select(year);
		sel1.selectByValue(input);
		return input;
	}

	public String CAC(String input) {
		CAC.sendKeys(input);
		return input;
	}

	public void savevBtn() {
		svBtn.click();
	}

	public void submitBtn() {
		SubmitBtn.click();
	}
	
	public void parentExBtn() {
		parentExpand.click();
	}
	
	public void childEditBtn() {
		childEdit.click();
	}
	
	
	public void childCleartBtn() {
		childClear.click();
	}
	
	public void deleteChildBtn() {
		deleteChild.click();
	}
		

}
