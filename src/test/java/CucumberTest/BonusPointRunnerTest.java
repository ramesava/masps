package CucumberTest;

import org.junit.runner.RunWith;

import com.github.mkolisnyk.cucumber.runner.ExtendedCucumber;
import com.github.mkolisnyk.cucumber.runner.ExtendedCucumberOptions;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@RunWith(ExtendedCucumber.class)
@ExtendedCucumberOptions(jsonReport = "target/cucumber.json", detailedReport = true, detailedAggregatedReport = true, jsonUsageReport = "target/cucucmber-usage.json", overviewReport = true, toPDF = true, outputFolder = "target")
@CucumberOptions(strict = false, features = "src/test/resource/bonusPoint.feature", glue = { "stepDefinition" }, format = {
		"pretty", "html:target/HTML_result", "junit:target/result.xml", "json:target/cucumberBO.json" }, monochrome = true, tags = "@bonusPoint")
public class BonusPointRunnerTest extends AbstractTestNGCucumberTests {

}